#include <stdint.h>
#include <memory>
#include <string>

#include "OpenFlowMessage.hpp"
#include "RoscoOpenFlowMessage.hpp"

RoscoOpenFlowBLSShareMessage::RoscoOpenFlowBLSShareMessage(OpenFlowMessage& other) :
	OpenFlowMessage(std::move(other))
{
	xSignatureLength = ((struct RoscoOpenFlowBLSShareMessageBody*)xData->data())->xSignatureLength;
	xSignaturePtr = ((char*)xData->data()) + sizeof(struct RoscoOpenFlowBLSShareMessageBody);

	xMessageLength = getLength() - sizeof(struct RoscoOpenFlowBLSShareMessageBody) - xSignatureLength;
	xMessagePtr = xSignaturePtr + xSignatureLength;
}

RoscoOpenFlowBLSAggMessage::RoscoOpenFlowBLSAggMessage(OpenFlowMessage& other) :
	OpenFlowMessage(std::move(other))
{
	xSignatureLength = ((struct RoscoOpenFlowBLSAggMessageBody*)xData->data())->xSignatureLength;
	xSignaturePtr = ((char*)xData->data()) + sizeof(struct RoscoOpenFlowBLSAggMessageBody);

	xMessageLength = getLength() - sizeof(struct RoscoOpenFlowBLSAggMessageBody) - xSignatureLength;
	xMessagePtr = xSignaturePtr + xSignatureLength;
}
