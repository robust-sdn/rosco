#ifndef _ROSCOSIGNATUREMAP_H
#define _ROSCOSIGNATUREMAP_H

#include <stdint.h>
#include <unordered_map>
#include <vector>
#include <utility>
#include <mutex>

#include "RoscoTypes.hpp"
#include "blsUtil.hpp"
#include "RoscoOpenFlowMessage.hpp"

class RoscoBLSSignature
{
	public:
		RoscoBLSSignature(int i, const CharVectorPtr& s) :
			xNodeId(i), xSignature(s) {}

		int getNodeId() const { return xNodeId; }
		const CharVector& getSignature() const { return (*xSignature); }

	private:
		int xNodeId;
		CharVectorPtr xSignature;
};
typedef std::vector<RoscoBLSSignature> RoscoBLSSignatureVector;

class RoscoBLSSignatureSet
{
	public:
		RoscoBLSSignatureSet() : xRetired(false) {} 
		void setMessage(const Pairing& e, const G1& public_key, const CharVectorPtr& m) {
			xMessage = m;
			xMessageSignature = bls_compute_signature(e, public_key, m->data(), m->size());
		}
		void addSignature(int n, const CharVectorPtr& s) { xSignatures.emplace_back(n, s); }
		CharVectorPtr& getMessage() { return xMessage; }
		const GTPtr& getMessageSignature() const { return xMessageSignature; }
		const RoscoBLSSignatureVector& getSignatures() const { return xSignatures; }
		std::mutex& getLock() { return xLock; }
		bool isRetired() const { return xRetired; }
		void retire() { xRetired = true; }
		void clear() {
			xMessage.reset();
			xMessageSignature.reset();
			xSignatures.clear();
		}

	private:
		bool xRetired;
		std::mutex xLock;
		CharVectorPtr xMessage;
		GTPtr xMessageSignature;
		RoscoBLSSignatureVector xSignatures;
};
typedef std::unordered_map<uint32_t, RoscoBLSSignatureSet>::iterator RoscoBLSSignatureMapIterator;

class RoscoBLSSignatureMap
{
	public:
		RoscoBLSSignatureSet& addMessage(const Pairing& e, const G1& public_key, const RoscoOpenFlowBLSShareMessage& m) {
			xInsertLock.lock();
			RoscoBLSSignatureSet& sig_set = xMessageMap[m.getXID()];
			xInsertLock.unlock();
			sig_set.getLock().lock();
			if(!sig_set.isRetired()) {
				if(!sig_set.getMessage()) {
					sig_set.setMessage(e, public_key, m.getMessageBytes());
				}
				sig_set.addSignature(m.getNodeId(), m.getSignature());
			}
			return sig_set;
		}
		void removeMessage(RoscoBLSSignatureMapIterator itr) {
			xMessageMap.erase(itr);
		}

	private:
		std::mutex xInsertLock;
		std::unordered_map<uint32_t, RoscoBLSSignatureSet> xMessageMap;

};

#endif // _ROSCOSIGNATUREMAP_H
