#include <stdint.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <system_error>
#include <cstddef>
#include <vector>
#include <memory>
#include <mutex>

#include "util.hpp"
#include "exception.hpp"
#include "RoscoTypes.hpp"
#include "OpenFlowMessage.hpp"
#include "OpenFlowMessageReader.hpp"

OpenFlowMessagePtr OpenFlowMessageReader::read()
{
	std::lock_guard<std::mutex> read_lock(xReadMutex);
	CharVectorPtr msg = std::make_unique<CharVector>(OF_HEADER_SIZE);
	if(readWait((char*)msg->data(), 0, OF_HEADER_SIZE) > 0) {
		return nullptr;
	}
	uint16_t msg_size = ntohs(((struct OpenFlowHeader*)msg->data())->xLength);
	if(msg_size > OF_HEADER_SIZE) {
		msg->resize(msg_size);
		if(readWait(((char*)(msg->data()) + OF_HEADER_SIZE), 0, msg_size - OF_HEADER_SIZE) > 0) {
			return nullptr;
		}
	}
	return std::make_unique<OpenFlowMessage>(std::move(msg));
}

int OpenFlowMessageReader::readWait(char* buf, off_t offset, size_t count)
{
	char *p = buf;
	size_t remaining_to_read = count;
	int dest_offset = offset;
	while(remaining_to_read > 0) {
		dest_offset = ::read(xFd, p + dest_offset, remaining_to_read);
		if(dest_offset < 0) {
			PRINT_ERROR;
			return 1;
		}
		if(dest_offset == 0) {
			break;
		}
		remaining_to_read -= dest_offset;
	}
	return remaining_to_read;
}
