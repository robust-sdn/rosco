#ifndef _BUFUTIL_H
#define _BUFUTIL_H

#include <stdint.h>
#include <errno.h>
#include <sstream>
#include <system_error>

static inline uint8_t buf_getuint8(std::stringbuf& buf)
{
	uint8_t rv = buf.sbumpc();
	if(rv == EOF) {
		//throw std::system_error(EIO, std::generic_category(), "Buffer Underrun");
		
	}
	return rv;
}

static inline uint16_t buf_getuint16(std::stringbuf& buf)
{
	uint16_t rv;
	if(buf.sgetn((char*)&rv, sizeof(uint16_t)) != sizeof(uint16_t)) {
		//throw std::system_error(EIO, std::generic_category(), "Buffer Underrun");
	}
	return rv;
}

static inline uint32_t buf_getuint32(std::stringbuf& buf)
{
	uint32_t rv;
	if(buf.sgetn((char*)&rv, sizeof(uint32_t)) != sizeof(uint32_t)) {
		//throw std::system_error(EIO, std::generic_category(), "Buffer Underrun");
	}
	return rv;
}

static inline uint64_t buf_getuint64(std::stringbuf& buf)
{
	uint64_t rv;
	if(buf.sgetn((char*)&rv, sizeof(uint64_t)) != sizeof(uint64_t)) {
		//throw std::system_error(EIO, std::generic_category(), "Buffer Underrun");
	}
	return rv;
}

static inline void buf_getn(std::stringbuf& buf, char* out_buf, size_t n)
{
	if(buf.sgetn(out_buf, n) != n) {
		//throw std::system_error(EIO, std::generic_category(), "Buffer Underrun");
	}
}

static inline void buf_putuint8(std::stringbuf& buf, uint8_t val)
{
	buf.sputc((char)val);
}

static inline void buf_putuint16(std::stringbuf& buf, uint16_t val)
{
	buf.sputn((const char*)&val, sizeof(uint16_t));
}
		
static inline void buf_putuint32(std::stringbuf& buf, uint32_t val)
{
	buf.sputn((const char*)&val, sizeof(uint32_t));
}

static inline void buf_putuint64(std::stringbuf& buf, uint64_t val)
{
	buf.sputn((const char*)&val, sizeof(uint64_t));
}

#endif // _BUFUTIL_H
