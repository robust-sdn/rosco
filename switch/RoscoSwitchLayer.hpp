#ifndef _OPENFLOWSERVER_H
#define _OPENFLOWSERVER_H

#include <thread>
#include <vector>
#include <string>
#include <memory>
#include <atomic>
#include <mutex>
#include <condition_variable>

#include "RoscoProperties.hpp"
#include "OpenFlowConnectionProxy.hpp"
#include "RoscoSwitchMessageHandler.hpp"

class RoscoSwitchLayer
{
	public:
		RoscoSwitchLayer(const RoscoProperties& props, const char* pairing_path); 
		~RoscoSwitchLayer();
		void start();
		void stop();
		void waitDone();

		std::vector<OpenFlowConnectionProxyPtr>& getAllConnections() { return xConnections; }

	private:
		class ServerInfo {
			public:
				int xIndex;
				int xSocketFd;
				std::string xTargetHost;
				std::string xTargetPort;

				ServerInfo(int i, std::string h, std::string p) :
					xIndex(i), xSocketFd(-1), xTargetHost(h), xTargetPort(p) {}
		};
		typedef std::unique_ptr<ServerInfo> ServerInfoPtr;

		void disconnectOVS();
		void connectOVS();
		void stopUnlocked();
		void runLoop(int);

		bool xRunning;
		std::condition_variable xReadyCondition;
		std::atomic_int xReadyCount;
		std::vector<ServerInfoPtr> xServerInfo;
		std::vector<std::thread> xServerThreads;
		std::mutex xConnectionMutex;
		std::vector<OpenFlowConnectionProxyPtr> xConnections;
		std::unique_ptr<RoscoSwitchMessageHandler> xMessageHandler;
};

#endif // _OPENFLOWSERVER_H
