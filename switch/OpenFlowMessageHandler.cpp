#include <iostream>

#include "OpenFlowMessage.hpp"
#include "OpenFlowConnectionProxy.hpp"
#include "OpenFlowMessageHandler.hpp"

void OpenFlowEchoHandler::handleMessageFromSwitch(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg)
{
	std::cout << "MSG FROM SWITCH: " << conn.getSwitchPeer() << " : " << msg << std::endl;
	conn.sendController(msg);
}

void OpenFlowEchoHandler::handleMessageFromController(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg)
{
	std::cout << "MSG FROM CONTROLLER: " << conn.getControllerPeer() << " : " << msg << std::endl;
	conn.sendSwitch(msg);
}
