#ifndef _OPENFLOWCONNECTION_H
#define _OPENFLOWCONNECTION_H

#include <thread>
#include <mutex>
#include <string>
#include <memory>

#include "OpenFlowMessage.hpp"
#include "OpenFlowMessageWriter.hpp"
#include "OpenFlowMessageReader.hpp"

class OpenFlowConnection;

class OpenFlowConnectionCallback
{
	public:
		virtual void callback(OpenFlowConnection* conn, OpenFlowMessagePtr& msg) = 0;
};

class OpenFlowConnection
{
	public:
		OpenFlowConnection();
		OpenFlowConnection(int fd); 
		~OpenFlowConnection();

		void connect(const std::string& host, const std::string& port);
		void closeConnection();
		void sendMessage(const OpenFlowMessage& msg);
		void sendBytes(const void* buf, size_t size);
		void start(OpenFlowConnectionCallback* callback);
		void stop();
		void waitDone();
		uint8_t getVersion() const { return xProtocolVersion; }
		const std::string& getPeerName() const { return xPeerName; }
	
	private:
		bool xRunning;
		bool xConnected;
		int xSocketFd;
		uint8_t xProtocolVersion;
		std::unique_ptr<std::thread> xConnectionThread;
		std::mutex xConnectionMutex;
		std::string xPeerName;
		std::unique_ptr<OpenFlowMessageWriter> xWriter;
		std::unique_ptr<OpenFlowMessageReader> xReader;
		OpenFlowConnectionCallback* xCallback;
		
		void runLoop();
		void setConnectionName();
};

typedef std::unique_ptr<OpenFlowConnection> OpenFlowConnectionPtr;

#endif // _OPENFLOWCONNECTION_H
