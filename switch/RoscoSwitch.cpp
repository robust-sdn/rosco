#include <stdio.h>
#include <fcntl.h>
#include <iostream>
#include <unistd.h>
#include <signal.h>

#include "RoscoSwitchLayer.hpp"

std::function<void(int)> shutdown_handler;
void signal_handler(int signal)
{
	shutdown_handler(signal);
}

int main(int argc, char* argv[])
{
	std::string config_path(argv[1]);
	std::string props_path = config_path + "/rosco.config";
	std::string pairing_path = config_path + "/pairing.param";
	RoscoProperties props(props_path.c_str(), 0);
	RoscoSwitchLayer switchLayer(props, pairing_path.c_str());

	shutdown_handler = [&](int signal) {
		switchLayer.stop();
	};

	signal(SIGINT, signal_handler);

	switchLayer.start();
	switchLayer.waitDone();
	return 0;
}
