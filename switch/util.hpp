#ifndef _UTIL_H
#define _UTIL_H

#include <stdint.h>
#include <pthread.h>
#include <iomanip>
#include <sstream>
#include <string>
#include <system_error>

#define NSEC_PER_SEC 1000000000
static inline uint64_t get_time_ns()
{
	struct timespec ts;
	if(clock_gettime(CLOCK_REALTIME, &ts) == -1) {
		throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
	}
	return ts.tv_sec * NSEC_PER_SEC + ts.tv_nsec;
}
#undef NSEC_PER_SEC

static inline std::string string_to_hex(const std::string& s, bool upper_case = true)
{
	std::ostringstream ret;
	for(std::string::size_type i = 0; i < s.length(); ++i) {
		ret << std::hex << std::setfill('0') << std::setw(2) << (upper_case ? std::uppercase : std::nouppercase) << (unsigned int)s[i];
	}
	return ret.str();
}

static inline void do_mutex_lock(pthread_mutex_t* mutex)
{
	if(pthread_mutex_lock(mutex) == -1) {
		throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
	}
}

static inline void do_mutex_unlock(pthread_mutex_t* mutex)
{
	if(pthread_mutex_unlock(mutex) == -1) {
		throw std::system_error(std::make_error_code(static_cast<std::errc>(errno)));
	}
}

namespace std {
	template<typename T, typename... Args>
	static inline std::unique_ptr<T> make_unique(Args&&... args) {
			return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
	}
};

#endif // _UTIL_H
