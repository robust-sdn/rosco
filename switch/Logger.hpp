#ifndef _LOGGER_H
#define _LOGGER_H

#include <iostream>
#include <sstream>
#include <mutex>
#include <thread>
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <iomanip> // put_time

class Logger : public std::ostringstream
{
	public:
	    Logger() = default;
		~Logger() {
			std::lock_guard<std::mutex> guard(xPrintMutex);
			auto now = std::chrono::system_clock::now();
		    auto in_time_t = std::chrono::system_clock::to_time_t(now);
			std::cout << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %X") << " : "
				<< std::this_thread::get_id() << " : " << this->str() << std::flush;
		}

	private:
		static std::mutex xPrintMutex;
};

#endif  //_LOGGER_H
