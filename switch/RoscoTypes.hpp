#ifndef _ROSCOTYPES_H
#define _ROSCOTYPES_H

#include <vector>
#include <memory>

class G1;
class G2;
class GT;
class Zr;

typedef std::unique_ptr<G1> G1Ptr;
typedef std::unique_ptr<G2> G2Ptr;
typedef std::unique_ptr<GT> GTPtr;
typedef std::unique_ptr<Zr> ZrPtr;

typedef std::vector<char> CharVector;
typedef std::shared_ptr<CharVector> CharVectorPtr;

#endif // _ROSCO_TYPES_H
