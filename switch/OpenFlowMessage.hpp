#ifndef _OPENFLOWMESSAGE_H
#define _OPENFLOWMESSAGE_H

#include <stdint.h>
#include <arpa/inet.h>
#include <iostream>
#include <utility>
#include <memory>
#include <vector>

#include "RoscoTypes.hpp"

#define OF_HEADER_SIZE 8

#define OF_VERSION_10 1
#define OF_VERSION_11 2
#define OF_VERSION_12 3
#define OF_VERSION_13 4
#define OF_VERSION_14 5

#define OF_MSG_HELLO   0
#define OF_MSG_ERROR   1
#define OF_MSG_ECHOREQ 2
#define OF_MSG_ECHORES 3
#define OF_MSG_BARRIERRES_10 19
#define OF_MSG_BARRIERRES_11 21
#define OF_MSG_ROLEREQ 24
#define OF_MSG_ROLERES 25

#define OF_MSG_ROLE_NOCHANGE 0x00000000
#define OF_MSG_ROLE_EQUAL    0x00000001
#define OF_MSG_ROLE_MASTER   0x00000002
#define OF_MSG_ROLE_SLAVE    0x00000003

struct OpenFlowHeader
{
	uint8_t xVersion;
	uint8_t xType;
	uint16_t xLength;
	uint32_t xXID;
} __attribute__((packed));

class OpenFlowMessage
{
	protected:
		CharVectorPtr xData;

	public:
		OpenFlowMessage(uint8_t v, uint8_t t, uint16_t l, uint32_t x);
		OpenFlowMessage(const OpenFlowMessage& other) = delete;
		OpenFlowMessage(OpenFlowMessage&& other) : xData(std::move(other.xData)) {}
		OpenFlowMessage(CharVectorPtr msg);

		OpenFlowMessage& operator= (const OpenFlowMessage& other) = delete;
		OpenFlowMessage& operator= (OpenFlowMessage&& other) 
		{
			xData = std::move(other.xData);
		}

		static uint32_t getXID(const CharVectorPtr& msg) { return ntohl(((struct OpenFlowHeader*)msg->data())->xXID); }
		static void setXID(uint32_t x, CharVectorPtr& msg) { ((struct OpenFlowHeader*)msg->data())->xXID = htonl(x); }

		void setVersion(uint8_t x) { ((struct OpenFlowHeader*)xData->data())->xVersion = x; }
		void setType(uint8_t x) { ((struct OpenFlowHeader*)xData->data())->xType = x; }
		void setLength(uint16_t x) { ((struct OpenFlowHeader*)xData->data())->xLength = htons(x); }
		void setXID(uint32_t x) { setXID(x, xData); }

		uint8_t getVersion() const { return ((struct OpenFlowHeader*)xData->data())->xVersion; }
		uint8_t getType() const { return ((struct OpenFlowHeader*)xData->data())->xType; }
		uint16_t getLength() const { return ntohs(((struct OpenFlowHeader*)xData->data())->xLength); }
		uint32_t getXID() const { return getXID(xData); }
		const std::vector<char>& getBytes() const { return (*xData); }
};

struct OpenFlowRoleRequestMessageBody
{
	struct OpenFlowHeader xHeader;
	uint32_t xRole;
	uint32_t xPad;
	uint64_t xGenerationId;
} __attribute__((packed));

class OpenFlowRoleRequestMessage : public OpenFlowMessage
{
	public:
		OpenFlowRoleRequestMessage(uint8_t v, uint32_t x, uint32_t r, uint64_t g);
		OpenFlowRoleRequestMessage(const OpenFlowRoleRequestMessage& other) = delete;
		OpenFlowRoleRequestMessage(OpenFlowRoleRequestMessage&& other) : OpenFlowMessage(std::move(other)) {}
		OpenFlowRoleRequestMessage(CharVectorPtr msg);
		
		OpenFlowRoleRequestMessage& operator= (const OpenFlowRoleRequestMessage& other) = delete;
		OpenFlowRoleRequestMessage& operator= (OpenFlowRoleRequestMessage&& other) 
		{
			xData = std::move(other.xData);
		}
		
		void setRole(uint32_t x) { ((struct OpenFlowRoleRequestMessageBody*)xData->data())->xRole = htonl(x); }
		void setGenerationId(uint64_t x) { ((struct OpenFlowRoleRequestMessageBody*)xData->data())->xGenerationId = htobe64(x); }

		uint32_t getRole() const { return ntohl(((struct OpenFlowRoleRequestMessageBody*)xData->data())->xRole); }
		uint64_t getGenerationId() const { return be64toh(((struct OpenFlowRoleRequestMessageBody*)xData->data())->xGenerationId); }
};

class OpenFlowBarrierResponseMessage : public OpenFlowMessage
{
	public:
		OpenFlowBarrierResponseMessage(uint8_t v, uint32_t x) :
			OpenFlowMessage(v, v < OF_VERSION_11 ? OF_MSG_BARRIERRES_10 : OF_MSG_BARRIERRES_11, OF_HEADER_SIZE, x) {}
		OpenFlowBarrierResponseMessage(const OpenFlowBarrierResponseMessage& other) = delete;
		OpenFlowBarrierResponseMessage(OpenFlowBarrierResponseMessage&& other) :
			OpenFlowMessage(std::move(other)) {}
		OpenFlowBarrierResponseMessage(CharVectorPtr msg) :
			OpenFlowMessage(std::move(msg)) {}

		OpenFlowBarrierResponseMessage& operator= (const OpenFlowBarrierResponseMessage& other) = delete;
		OpenFlowBarrierResponseMessage& operator= (OpenFlowBarrierResponseMessage&& other)
		{
			xData = std::move(other.xData);
		}
};

typedef std::unique_ptr<OpenFlowMessage> OpenFlowMessagePtr;
std::ostream& operator << (std::ostream& os, const OpenFlowMessage& msg);
std::ostream& operator << (std::ostream& os, const OpenFlowRoleRequestMessage& msg);

#endif // _OPENFLOWMESSAGE_H
