#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <thread>
#include <system_error>
#include <atomic>
#include <condition_variable>
#include <mutex>

#include "util.hpp"
#include "exception.hpp"
#include "RoscoSwitchLayer.hpp"
#include "RoscoProperties.hpp"
#include "OpenFlowConnection.hpp"
#include "OpenFlowMessageHandler.hpp"

#define BASE_SOCKET_PATH "/var/run/openvswitch"
#define BASE_SOCKET_NAME "rosco_switch_"
#define MAX_BACKLOG 10

RoscoSwitchLayer::RoscoSwitchLayer(const RoscoProperties& props, const char* pairing_path) :
	xRunning(false),  xReadyCount(0)
{
	std::string controller_port = props.getProperty("LISTENPORT");
	int controller_count = 0;
	while(1) {
		try {
			std::string target_host = props.getProperty(std::string("CONTROLLER.") + std::to_string(controller_count+1));
			xServerInfo.push_back(ServerInfoPtr(new ServerInfo(controller_count, target_host, controller_port)));
			controller_count++;
		} catch(std::system_error e) {
			break;
		}
	}
	if(pairing_path != NULL) {
		xMessageHandler.reset(new RoscoSwitchBLSMessageHandler((*this), props, controller_count, pairing_path));
	}
}

RoscoSwitchLayer::~RoscoSwitchLayer()
{
	if(xRunning) {
		stop();
		waitDone();
	}
}

void RoscoSwitchLayer::start()
{
	std::unique_lock<std::mutex> lock(xConnectionMutex);
	disconnectOVS();
	if(xRunning) {
		stopUnlocked();
	}
	xReadyCount = xServerInfo.size();
	xRunning = true;

	for(std::vector<ServerInfoPtr>::iterator itr = xServerInfo.begin(); itr != xServerInfo.end(); itr++) {
		xServerThreads.push_back(std::thread(&RoscoSwitchLayer::runLoop, this, (*itr)->xIndex));
	}
	
	while(xReadyCount > 0) {
		xReadyCondition.wait(lock);
	}

	if(xRunning) {
		connectOVS();
	}
}

void RoscoSwitchLayer::disconnectOVS()
{
	std::cout << "DISCONNECTING OVS" << std::endl;
	system("sudo ovs-vsctl set-controller br0");
}

void RoscoSwitchLayer::connectOVS()
{
	std::string target_cmd("sudo ovs-vsctl set-controller br0");
	for(std::vector<ServerInfoPtr>::iterator itr = xServerInfo.begin(); itr != xServerInfo.end(); itr++) {
		target_cmd.append(" unix:" BASE_SOCKET_PATH "/" BASE_SOCKET_NAME).append((*itr)->xTargetHost);
	}
	std::cout << "CONNECTING OVS" << std::endl;
	system(target_cmd.c_str());
}

void RoscoSwitchLayer::stopUnlocked()
{
	xRunning = false;
	for(std::vector<ServerInfoPtr>::iterator itr = xServerInfo.begin(); itr != xServerInfo.end(); itr++) {
		shutdown((*itr)->xSocketFd, SHUT_RDWR);
	}
	disconnectOVS();
}

void RoscoSwitchLayer::stop()
{
	std::lock_guard<std::mutex> lock(xConnectionMutex);
	stopUnlocked();
}

void RoscoSwitchLayer::waitDone()
{
	for(std::vector<std::thread>::iterator itr = xServerThreads.begin(); itr != xServerThreads.end(); itr++) {
		if(itr->joinable()) {
			itr->join();
		}
	}
}

void RoscoSwitchLayer::runLoop(int index)
{
	ServerInfoPtr& myInfo = xServerInfo[index];
	bool ready_signaled = false;

	try {
		// Creating socket file descriptor 
		if((myInfo->xSocketFd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
			PRINT_ERRNO_AND_THROW(errno);
	   	} 

		// Set up domain socket
		struct sockaddr_un address;
		memset(&address, 0, sizeof(struct sockaddr_un));
		int addrlen = sizeof(address); 
		address.sun_family = AF_UNIX;
		snprintf(address.sun_path, sizeof(address.sun_path), BASE_SOCKET_PATH "/" BASE_SOCKET_NAME "%s",  myInfo->xTargetHost.c_str());
		unlink(address.sun_path);
    
	   	// Forcefully attaching socket to the port
		if(bind(myInfo->xSocketFd, (struct sockaddr *)&address, sizeof(address)) == -1) { 
			PRINT_ERRNO_AND_THROW(errno);
		}
		if(listen(myInfo->xSocketFd, MAX_BACKLOG) == -1) {
			PRINT_ERRNO_AND_THROW(errno);
		}
		
		std::cout << index << " : READY TO PROCESS CONNECTIONS" << std::endl;
	
		xReadyCount.fetch_sub(1);
		xReadyCondition.notify_one();
		ready_signaled = true;
	
		while(xRunning) {
			int client_socket = 0;
			if ((client_socket = accept(myInfo->xSocketFd, (struct sockaddr*)&address, (socklen_t*)&addrlen)) == -1) {
				continue;
			}
			std::lock_guard<std::mutex> connection_guard(xConnectionMutex);
			OpenFlowConnectionPtr clientConn(new OpenFlowConnection(client_socket));
			std::cout << index << ": NEW CLIENT CONNECTION: " << clientConn->getPeerName() << std::endl;;
	
			OpenFlowConnectionPtr targetConn = std::make_unique<OpenFlowConnection>();
			targetConn->connect(myInfo->xTargetHost, myInfo->xTargetPort);
			OpenFlowConnectionProxyPtr newConn = std::make_unique<OpenFlowConnectionProxy>(targetConn, clientConn, (*xMessageHandler), index);
			newConn->openConnection();
			xConnections.push_back(std::move(newConn));
		}
		close(myInfo->xSocketFd);
	} catch(const system_error& e) {
		if(!ready_signaled) {
			xReadyCount.fetch_sub(1);
			xReadyCondition.notify_one();
		}
		stop();
	}
}
