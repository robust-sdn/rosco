#ifndef _OPENFLOWCONNECTIONPROXY_H
#define _OPENFLOWCONNECTIONPROXY_H

#include <stdint.h>
#include <utility>

#include "OpenFlowMessage.hpp"
#include "OpenFlowConnection.hpp"
#include "OpenFlowMessageHandler.hpp"

class OpenFlowConnectionProxy : public OpenFlowConnectionCallback
{
	public:
		OpenFlowConnectionProxy(OpenFlowConnectionPtr& cc, OpenFlowConnectionPtr& sc, OpenFlowMessageHandler& handler, int i) :
			xControllerConnection(std::move(cc)), xSwitchConnection(std::move(sc)), xMessageHandler(handler), xIndex(i) {}
		~OpenFlowConnectionProxy();

		void openConnection();
		void closeConnection();
		const std::string& getSwitchPeer() const { return xSwitchConnection->getPeerName(); }
		const std::string& getControllerPeer() const { return xControllerConnection->getPeerName(); }
		void sendSwitch(const OpenFlowMessage& msg);
		void sendSwitch(const void* buf, size_t size);
		void sendController(const OpenFlowMessage& msg);
		void sendController(const void* buf, size_t size);
		uint8_t getNegotiatedVersion() const;
		int getIndex() const { return xIndex; }
		void callback(OpenFlowConnection* conn, OpenFlowMessagePtr& msg);
	
	private:
		int xIndex;
		OpenFlowMessageHandler& xMessageHandler;
		OpenFlowConnectionPtr xControllerConnection;
		OpenFlowConnectionPtr xSwitchConnection;
};

typedef std::unique_ptr<OpenFlowConnectionProxy> OpenFlowConnectionProxyPtr;

#endif // _OPENFLOWCONNECTIONPROXY_H
