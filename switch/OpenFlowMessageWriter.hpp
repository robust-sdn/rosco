#ifndef _OPENFLOWMESSAGEWRITER_H
#define _OPENFLOWMESSAGEWRITER_H

#include <vector>
#include <mutex>

#include "OpenFlowMessage.hpp"

class OpenFlowMessageWriter
{
	public:
		OpenFlowMessageWriter(int fd) : xFd(fd) {}
		bool writeBytes(const void* buffer, size_t size);
		bool write(const OpenFlowMessage& msg) { 
			const std::vector<char>& msgBytes = msg.getBytes();
			return writeBytes(msgBytes.data(), msgBytes.size());
		}

	private:
		int xFd;
		std::mutex xWriteMutex;
};

#endif // _OPENFLOWMESSAGEWRITER_H
