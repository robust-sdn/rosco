#ifndef _OPENFLOWMESSAGEREADER_H
#define _OPENFLOWMESSAGEREADER_H

#include <mutex>
#include "OpenFlowMessage.hpp"

class OpenFlowMessageReader
{
	public:
		OpenFlowMessageReader(int fd) : xFd(fd) {}
		OpenFlowMessagePtr read();

	private:
		int xFd;
		std::mutex xReadMutex;
		int readWait(char* buf, off_t offset, size_t count);
};

#endif // _OPENFLOWMESSAGEREADER_H
