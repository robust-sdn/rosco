#ifndef _OPENFLOWMESSAGEHANDLER_H
#define _OPENFLOWMESSAGEHANDLER_H

//#include "OpenFlowConnectionProxy.hpp"

class OpenFlowConnectionProxy;

class OpenFlowMessageHandler
{
	public:
		virtual void handleMessageFromSwitch(OpenFlowConnectionProxy& con, OpenFlowMessage& msg) = 0;
		virtual void handleMessageFromController(OpenFlowConnectionProxy& con, OpenFlowMessage& msg) = 0;
};

class OpenFlowEchoHandler : public OpenFlowMessageHandler
{
	public:
		void handleMessageFromSwitch(OpenFlowConnectionProxy& con, OpenFlowMessage& msg);
		void handleMessageFromController(OpenFlowConnectionProxy& con, OpenFlowMessage& msg);
};

typedef std::shared_ptr<OpenFlowMessageHandler> OpenFlowMessageHandlerPtr;

#endif // _OPENFLOWMESSAGEHANDLER_H
