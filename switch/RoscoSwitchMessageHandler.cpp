#include <errno.h>
#include <stdio.h>
#include <memory>
#include <iostream>

#include "OpenFlowMessage.hpp"
#include "OpenFlowConnectionProxy.hpp"
#include "OpenFlowMessageHandler.hpp"

#include "util.hpp"
#include "blsUtil.hpp"
#include "Logger.hpp"
#include "RoscoSwitchLayer.hpp"
#include "RoscoOpenFlowMessage.hpp"
#include "RoscoSwitchMessageHandler.hpp"

#define IS_BLS_SHARE(type) (type == ROSCO_MSG_BLS)
#define IS_BLS_AGG(type) (type == ROSCO_MSG_BLS_AGG || type == ROSCO_MSG_BLS_AGG_ROLE)
#define IS_ROSCO_MSG(type) (IS_BLS_AGG(type) || IS_BLS_SHARE(type))

static inline int compute_quorum_size(int n)
{
	return ceil((((double)n) + 1) / 2);
}

RoscoSwitchMessageHandler::RoscoSwitchMessageHandler(RoscoSwitchLayer& sl, int c) :
	xSwitchLayer(sl), xQuorumSize(compute_quorum_size(c)), xSentRoscoRoleChange(false), xSentRoscoRoleXID(0), xPrimaryConnection(NULL), xNextXID(0) {}

void RoscoSwitchMessageHandler::sendAck(const OpenFlowMessage& msg)
{
	OpenFlowBarrierResponseMessage ack_msg(msg.getVersion(), msg.getXID());
	std::vector<OpenFlowConnectionProxyPtr>& connections = xSwitchLayer.getAllConnections();
	for(std::vector<OpenFlowConnectionProxyPtr>::iterator itr = connections.begin(); itr != connections.end(); itr++) {
		//Logger{} << "SEND ACK TO: " << (*itr)->getSwitchPeer() << ":" << (*itr)->getIndex() << " : " << ack_msg << std::endl;
		(*itr)->sendController(ack_msg);
	}
}

bool RoscoSwitchMessageHandler::handleRoleRequest(OpenFlowConnectionProxy& primaryConn, OpenFlowRoleRequestMessage& role_msg)
{
	xSentRoscoRoleChange = true;
	xSentRoscoRoleXID = role_msg.getXID();
	xPrimaryConnection = &primaryConn;
	std::vector<OpenFlowConnectionProxyPtr>& switch_connections = xSwitchLayer.getAllConnections();
	OpenFlowRoleRequestMessage slave_msg(role_msg.getVersion(), role_msg.getXID(),
		OF_MSG_ROLE_SLAVE, role_msg.getGenerationId());

	Logger{} << "PRIMARY MESSAGE: " << role_msg << std::endl;
	Logger{} << "SLAVE MESSAGE: " << slave_msg << std::endl;

	primaryConn.sendSwitch(role_msg);
	for(std::vector<OpenFlowConnectionProxyPtr>::iterator itr = switch_connections.begin(); itr != switch_connections.end(); itr++) {
		if((*itr)->getIndex() != primaryConn.getIndex()) {
			(*itr)->sendSwitch(slave_msg);
		}
	}
	return true;
}

bool RoscoSwitchMessageHandler::handleRoleResponse(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg)
{
	if(xSentRoscoRoleChange && msg.getXID() == xSentRoscoRoleXID) {
		xSentRoscoRoleChange = false;
		return true;
	}
	return false;
}

RoscoSwitchBLSMessageHandler::RoscoSwitchBLSMessageHandler(RoscoSwitchLayer& sl, const RoscoProperties& props, int c, const char* pairing_path) :
	RoscoSwitchMessageHandler(sl, c),
	xPairing(std::unique_ptr<FILE, int(*)(FILE*)>(fopen(pairing_path, "r"), &fclose).get())
{
	if(!xPairing.isPairingPresent()) {
		throw std::system_error(EIO, std::generic_category(), "Create pairing failed");
	}
	std::string public_key_string = props.getProperty("PUBLICKEY");
	xPublicKey = G1(xPairing, (unsigned char*)public_key_string.c_str(), public_key_string.length(), false, 10);
	std::string u_string = props.getProperty("U");
	xU = G1(xPairing, (unsigned char*)u_string.c_str(), u_string.length(), false, 10);
}

void RoscoSwitchBLSMessageHandler::handleMessageFromSwitch(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg)
{
	/*
	if(msg.getType() != OF_MSG_ECHOREQ && msg.getType() != OF_MSG_ECHORES) {
		Logger{} << std::dec << "MSG FROM SWITCH: " << conn.getSwitchPeer() << ":" << conn.getIndex() << " : " << msg << std::endl;
	}
	*/
	if(msg.getType() == OF_MSG_ECHOREQ) {
		Logger{} << std::dec << "MSG FROM SWITCH: " << conn.getSwitchPeer() << ":" << conn.getIndex() << " : " << msg << std::endl;
		msg.setType(OF_MSG_ECHORES);
		conn.sendSwitch(msg);
		return;
	}
	if(msg.getType() == OF_MSG_ROLERES && handleRoleResponse(conn, msg)) {
		return;
	}
	msg.setXID(xNextXID.fetch_add(1));
	conn.sendController(msg);
}

void RoscoSwitchBLSMessageHandler::handleMessageFromController(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg)
{
	/*
	if(msg.getType() != OF_MSG_ECHOREQ && msg.getType() != OF_MSG_ECHORES) {
		Logger{} << "MSG FROM CONTROLLER: " << conn.getControllerPeer() << " : " << msg << std::endl;
	}
	*/
	if(msg.getType() == OF_MSG_ECHOREQ || msg.getType() == OF_MSG_ECHORES) {
		Logger{} << std::dec << "MSG FROM CONTROLLER: " << conn.getControllerPeer() << " : " << msg << std::endl;
	}
	switch(msg.getType()) {
		case ROSCO_MSG_BLS:
			handleBLSShareMessage(conn, msg);
			break;
		case ROSCO_MSG_BLS_AGG:
		case ROSCO_MSG_BLS_AGG_ROLE:
			handleBLSAggregateMessage(conn, msg);
			break;
		default:
			conn.sendSwitch(msg);
	}
}

void RoscoSwitchBLSMessageHandler::handleBLSAggregateMessage(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg)
{
	if(xRetiredXIDs.find(msg.getXID()) != xRetiredXIDs.end()) {
		sendAck(msg);
		return;
	}
	RoscoOpenFlowBLSAggMessage rosco_msg(msg);
	const CharVectorPtr signature = rosco_msg.getSignature();
	const CharVectorPtr message = rosco_msg.getMessageBytes();
	if(bls_verify_signature(xPairing, xPublicKey, message->data(), message->size(), signature->data(), signature->size())) {
		xRetiredXIDs.insert(rosco_msg.getXID());
		sendAck(rosco_msg);
		if(rosco_msg.getType() == ROSCO_MSG_BLS_AGG_ROLE) {
			OpenFlowRoleRequestMessage role_msg(message);
			handleRoleRequest(conn, role_msg);
		} else {
			OpenFlowConnectionProxy* send_conn = xPrimaryConnection ? xPrimaryConnection : &conn;
			send_conn->sendSwitch((void*)message->data(), message->size());
		}
	}
}

void RoscoSwitchBLSMessageHandler::handleBLSShareMessage(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg)
{
	RoscoOpenFlowBLSShareMessage rosco_msg(msg);
	RoscoBLSSignatureSet& sig_set = xSignatureMap.addMessage(xPairing, xPublicKey, rosco_msg);
	if(sig_set.isRetired()) {
		sig_set.getLock().unlock();
		return;
	}
	if(sig_set.getSignatures().size() >= xQuorumSize) {
		const GTPtr& msg_signature = sig_set.getMessageSignature();
		if(bls_verify_signatures(xPairing, xU, (*msg_signature), sig_set)) {
			sig_set.retire();
			sig_set.getLock().unlock();
			CharVectorPtr& message = sig_set.getMessage();
			OpenFlowMessage::setXID(OpenFlowMessage::getXID(message) & 0x7FFFFFFF, message);
			//Logger{} << "SENDING VALIDATED MSG TO SWITCH: " << rosco_msg.getXID() << " : " << conn.getSwitchPeer() << " : " << conn.getIndex() << std::endl;
			conn.sendSwitch((char*)message->data(), message->size());
			sig_set.clear();
		}
	}
	sig_set.getLock().unlock();
}
