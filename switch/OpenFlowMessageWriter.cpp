#include <unistd.h>
#include <mutex>

#include "util.hpp"
#include "exception.hpp"
#include "OpenFlowMessage.hpp"
#include "OpenFlowMessageWriter.hpp"

bool OpenFlowMessageWriter::writeBytes(const void* buff, size_t size)
{
	std::lock_guard<std::mutex> write_lock(xWriteMutex);
	ssize_t rv = ::write(xFd, buff, size);
	if(rv == -1) {
		PRINT_ERROR;
		return false;
	}
	if(rv != size) {
		return false;
	}
	return true;
}
