#ifndef BLS_UTILS_H
#define BLS_UTILS_H

#include "RoscoTypes.hpp"

class Pairing;
class G1;

class RoscoBLSSignatureSet;

GTPtr bls_compute_signature(const Pairing& e, const G1& public_key, const void* msg, size_t msg_len);
bool bls_verify_signature(const Pairing& e, const G1& public_key,  const void* msg, size_t msg_len, const void* msg_signature, size_t sig_len);
bool bls_verify_signatures(const Pairing& e, const G1& u, const GT& msg_pub_sig, const RoscoBLSSignatureSet& signatures);

#endif // BLS_UTILS_H
