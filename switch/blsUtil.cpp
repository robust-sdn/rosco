#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <gcrypt.h>
#include <vector>
#include <string>
#include <memory>

#include "util.hpp"
#include "RoscoTypes.hpp"
#include "RoscoBLSSignatureMap.hpp"
#include "blsUtil.hpp"
#include "PBC/PBC.h"

// #include "lagrange.h"

//#define DEBUG 1
#define DEBUG_OUT stdout

#ifdef DEBUG
#include <inttypes.h>
#endif

typedef std::vector<Zr> ZrVector;
typedef std::unique_ptr<ZrVector> ZrVectorPtr;

/*
struct bls_signature*
bls_sign_message(const Pairing& e, const void* msg, size_t msg_len, const char* sig_share, size_t share_len)
{
	struct bls_signature* retval = NULL;
	string signatureString;

	try {
		Zr share(e, (unsigned char*)sig_share, share_len, 10);
#ifdef DEBUG
		share.dump(DEBUG_OUT, (char*)"Share: ", 10);
#endif

		unsigned char hash_buf[20];
		gcry_md_hash_buffer(GCRY_MD_SHA1, hash_buf, msg, msg_len);
		G1 msgHashG1(e, (void*)hash_buf, sizeof(hash_buf));
#ifdef DEBUG
		msgHashG1.dump(DEBUG_OUT, (char*)"Message Hash: ", 10);
#endif

		G1 signatureShare = msgHashG1^share;
#ifdef DEBUG
		signatureShare.dump(DEBUG_OUT, (char*)"Signature share: ", 10);
#endif
		signatureString = signatureShare.toString(false);
	} catch(runtime_error e) {
		fprintf(DEBUG_OUT, "Runtime Error recevied in bls_sign_message: %s\n", e.what());
		return NULL;
	}

	if (signatureString.size() <= 0) {
		fprintf(DEBUG_OUT, "Signature string size <= 0\n");
		return NULL;
	}

	retval = (struct bls_signature*)malloc(sizeof(struct bls_signature));
	retval->signature_length = signatureString.size();
	retval->signature = malloc(retval->signature_length);
	memcpy(retval->signature, signatureString.data(), retval->signature_length);

	return retval;
}
*/

static inline ZrVectorPtr lagrange_coeffs(const ZrVector& indices, const Zr& alpha)
{
    // This could be optimized, but this will do for now
  Zr numer, denom;
  ZrVectorPtr coeffs = std::make_unique<ZrVector>();
  for (size_t i = 0; i < indices.size(); ++i) {
	numer = Zr(alpha,(long int)1); 
	denom = Zr(alpha,(long int)1);
	for (size_t j = 0; j < indices.size(); ++j) {
	  if (j == i) continue;
	  numer *= (indices[j] - alpha);
	  denom *= (indices[j] - indices[i]);
	}
	coeffs->push_back(numer/denom);
  }
  return coeffs;
}

static inline G1Ptr lagrange_apply(const ZrVector& coeffs, const std::vector<G1>& shares)
{
  G1Ptr falpha = std::make_unique<G1>(shares[0],true);
  for (size_t i = 0; i < coeffs.size(); ++i) {
	(*falpha) *= shares[i]^coeffs[i]; 
  }
  return falpha;
}

static inline ZrPtr lagrange_apply(const ZrVector& coeffs, const ZrVector& shares){
  ZrPtr falpha = std::make_unique<Zr>(shares[0],(long)0);
  for (size_t i = 0; i < coeffs.size(); ++i) {
	(*falpha) += shares[i]*coeffs[i]; 
  }
  return falpha;	
}

GTPtr bls_compute_signature(const Pairing& e, const G1& public_key, const void* msg, size_t msg_len)
{
#ifdef DEBUG
	fprintf(DEBUG_OUT, "ENT: bls_compute_signature: %p %lu\n", msg, msg_len);
	uint64_t start_time = get_time_ns();
#endif
	unsigned char hash_buf[20];
	gcry_md_hash_buffer(GCRY_MD_SHA1, hash_buf, msg, msg_len);
	G1 msgHashG1(e, (void*)hash_buf, sizeof(hash_buf));
#ifdef DEBUG
	msgHashG1.dump(DEBUG_OUT, (char*)"Message Hash: ", 10);
#endif

	GTPtr signature = std::make_unique<GT>(e(public_key, msgHashG1));
#ifdef DEBUG
	signature->dump(DEBUG_OUT, (char*)"Message Signature: ", 10); 
#endif

#ifdef DEBUG
	uint64_t end_time = get_time_ns();
	fprintf(DEBUG_OUT, "Message Signature Time: %" PRIu64 "\n", end_time - start_time);
	fprintf(DEBUG_OUT, "EXT: bls_compute_signature\n");
#endif
	return signature;
}

static inline G1Ptr bls_aggregate_signatures(const Pairing& e, const RoscoBLSSignatureVector& signatures)
{
	ZrVector indices;
	std::vector<G1> shares;
	for(RoscoBLSSignatureVector::const_iterator itr = signatures.begin(); itr != signatures.end(); itr++) {
		int node_id = itr->getNodeId();
		const CharVector& signature = itr->getSignature();
#ifdef DEBUG
		char sigFileName[32];
		snprintf(sigFileName, sizeof(sigFileName), "/tmp/%d_sig.out", node_id);
		int sigFd = open(sigFileName, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);
		if(sigFd == -1) {
			fprintf(DEBUG_OUT, "Unable to open %s\n", sigFileName);
		} else {
			if(write(sigFd, signature.data(), signature.size()) == -1) {
				fprintf(DEBUG_OUT, "Failed to write signature to %s\n", sigFileName);
			}
			close(sigFd);
		}
#endif
		indices.push_back(Zr(e, (signed long)node_id));
		shares.push_back(G1(e, (unsigned char*)(signature.data()), signature.size(), false));
#ifdef DEBUG
		fprintf(DEBUG_OUT, "NODE ID: %d\n", node_id);
		shares.back().dump(DEBUG_OUT, (char*)"Signature share: ", 10);
#endif
	}
	//pushing evaluation at zero
	Zr alpha(e,(long)0);
	ZrVectorPtr coeffs = lagrange_coeffs(indices, alpha);
	return lagrange_apply(*coeffs, shares);
}

bool 
bls_verify_signatures(const Pairing& e, const G1& u, const GT& msg_pub_sig, const RoscoBLSSignatureSet& signatures)
{
#ifdef DEBUG
	uint64_t start_time = get_time_ns();
#endif
	G1Ptr msg_sig = bls_aggregate_signatures(e, signatures.getSignatures());
#ifdef DEBUG
	msg_pub_sig.dump(DEBUG_OUT, (char*)"Message Signature: ", 10);
	msg_sig->dump(DEBUG_OUT, (char*)"Aggregated Message Signature: ", 10);
#endif
	bool verified = e(u, (*msg_sig)) == msg_pub_sig;
#ifdef DEBUG
	uint64_t end_time = get_time_ns();
	fprintf(DEBUG_OUT, "Verify Time: %" PRIu64 "\n", end_time - start_time);
#endif
	return verified;
}

bool
bls_verify_signature(const Pairing& e, const G1& public_key, const void* msg, size_t msg_len, const void* msg_signature, size_t sig_len)
{
#ifdef DEBUG
	fprintf(DEBUG_OUT, "ENT: bls_verify_signature: %p %lu %p %lu\n", msg, msg_len, msg_signature, sig_len);
	uint64_t start_time = get_time_ns();
#endif
	GTPtr msg_public_sig = bls_compute_signature(e, public_key, msg, msg_len);
	GT msg_sig = GT(e, (unsigned char*)msg_signature, sig_len, 16);
#ifdef DEBUG
	msg_public_sig->dump(DEBUG_OUT, (char*)"Message Signature: ", 10);
	msg_sig.dump(DEBUG_OUT, (char*)"Sent Message Signature: ", 10);
#endif
	bool verified = msg_sig == (*msg_public_sig);
#ifdef DEBUG
	uint64_t end_time = get_time_ns();
	fprintf(DEBUG_OUT, "Verify Time: %" PRIu64 "\n", end_time - start_time);
#endif
	return verified;
}
