#ifndef _ROSCOSWITCHMESSAGEHANDLER_H
#define _ROSCOSWITCHMESSAGEHANDLER_H

#include <math.h>
#include <unordered_set>

#include "PBC/PBC.h"
#include "RoscoProperties.hpp"
#include "OpenFlowMessage.hpp"
#include "OpenFlowConnectionProxy.hpp"
#include "OpenFlowMessageHandler.hpp"
#include "RoscoBLSSignatureMap.hpp"

class RoscoSwitchLayer;
class RoscoOpenFlowBLSAggMessage;

class RoscoSwitchMessageHandler : public OpenFlowMessageHandler
{
	public:
		RoscoSwitchMessageHandler(RoscoSwitchLayer& sl, int c);
	
	protected:
		RoscoSwitchLayer& xSwitchLayer;
		int xQuorumSize;
		
		bool xSentRoscoRoleChange;
		uint32_t xSentRoscoRoleXID;
		OpenFlowConnectionProxy* xPrimaryConnection;
		std::unordered_set<uint32_t> xRetiredXIDs;
		std::atomic<uint32_t> xNextXID;
		
		void sendAck(const OpenFlowMessage& msg);
		bool handleRoleRequest(OpenFlowConnectionProxy& primaryConn, OpenFlowRoleRequestMessage& role_msg);
		bool handleRoleResponse(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg);
};

/*
class RoscoSwitchQuorumnMessageHandler : public RoscoSwitchMessageHandler
{
	public:
		RoscoSwitchBLSMessageHandler(RoscoSwitchLayer& sl, const RoscoProperties& props, int c);
		void handleMessageFromSwitch(OpenFlowConnectionProxy& con, OpenFlowMessage& msg);
		void handleMessageFromController(OpenFlowConnectionProxy& con, OpenFlowMessage& msg);

	private:
		int xQuorumSize;
		RoscoMessageMap xMessageMap;
};
*/

class RoscoSwitchBLSMessageHandler : public RoscoSwitchMessageHandler
{
	public:
		RoscoSwitchBLSMessageHandler(RoscoSwitchLayer& sl, const RoscoProperties& props, int c, const char* pairing_path);
		void handleMessageFromSwitch(OpenFlowConnectionProxy& con, OpenFlowMessage& msg);
		void handleMessageFromController(OpenFlowConnectionProxy& con, OpenFlowMessage& msg);

	protected:
		Pairing xPairing;
		G1 xU;
		G1 xPublicKey;
		RoscoBLSSignatureMap xSignatureMap;

		void handleBLSAggregateMessage(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg);
		void handleBLSShareMessage(OpenFlowConnectionProxy& conn, OpenFlowMessage& msg);
};

#endif // _ROSCOSWITCHMESSAGEHANDLER_H
