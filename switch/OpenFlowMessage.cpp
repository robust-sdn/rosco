#include <assert.h>
#include <stdint.h>
#include <endian.h>
#include <arpa/inet.h>
#include <sstream>
#include <iostream>

#include "util.hpp"
#include "RoscoTypes.hpp"
#include "OpenFlowMessage.hpp"
	
OpenFlowMessage::OpenFlowMessage(uint8_t v, uint8_t t, uint16_t l, uint32_t x) :
	xData(std::make_shared<CharVector>(l))
{
	assert(l >= OF_HEADER_SIZE);
	setVersion(v);
	setType(t);
	setLength(l);
	setXID(x);
}

OpenFlowMessage::OpenFlowMessage(CharVectorPtr msg) :
	xData(std::move(msg))
{
	assert(getLength() >= OF_HEADER_SIZE);
	assert(getLength() == xData->size());
}

OpenFlowRoleRequestMessage::OpenFlowRoleRequestMessage(uint8_t v, uint32_t x, uint32_t r, uint64_t g) :
	OpenFlowMessage(v, OF_MSG_ROLEREQ, OF_HEADER_SIZE + 16, x)
{
	assert(getVersion() >= OF_VERSION_12);
	setRole(r);
	setGenerationId(g);
}

OpenFlowRoleRequestMessage::OpenFlowRoleRequestMessage(CharVectorPtr msg) :
	OpenFlowMessage(std::move(msg))
{
	assert(getVersion() >= OF_VERSION_12);
	assert(getType() == OF_MSG_ROLEREQ);
}

std::ostream& operator << (std::ostream& os, const OpenFlowMessage& msg)
{
	os << std::dec <<
		"Version: " << (int)msg.getVersion() <<
		" Type: " << (int)msg.getType() << 
		" Length: " << msg.getLength() << 
		" XID: " << "0x" << std::uppercase << std::hex << msg.getXID();
	return os;
}

std::ostream& operator << (std::ostream& os, const OpenFlowRoleRequestMessage& msg)
{
	os << std::dec <<
		"Version: " << (int)msg.getVersion() <<
		" Type: " << (int)msg.getType() << 
		" Length: " << msg.getLength() << 
		" XID: " << "0x" << std::uppercase << std::hex << msg.getXID() <<
		" Role: " << "0x" << std::uppercase << std::hex << msg.getRole() <<
		" Generation ID: " << "0x" << std::uppercase << std::hex << msg.getGenerationId();
	return os;
}
