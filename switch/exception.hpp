#ifndef _EXCEPTION_H
#define _EXCEPTION_H

#include <sys/types.h>
#include <wait.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <execinfo.h>

#include <mutex>
#include <thread>
#include <iostream>
#include <system_error>

#define BT_SIZE 20

extern std::mutex xErrorPrintMutex;

#define PIPE_READ_END  0
#define PIPE_WRITE_END 1

#define _PRINT_LINE(TRACE, SYMBOL) \
{ \
	int p = 0; \
    while(SYMBOL[p] != '(' && SYMBOL[p] != ' ' && SYMBOL[p] != 0) ++p; \
	int pipefd[2]; \
	pipe(pipefd); \
	pid_t child = fork(); \
	if(child == 0) { \
		dup2(pipefd[PIPE_WRITE_END], STDOUT_FILENO); \
		dup2(pipefd[PIPE_WRITE_END], STDERR_FILENO); \
		close(pipefd[PIPE_READ_END]); \
		close(pipefd[PIPE_WRITE_END]); \
		char ptr[32]; snprintf(ptr, sizeof(ptr), "%p", TRACE); \
		char symbol[64]; snprintf(symbol, sizeof(symbol), "%.*s", p, SYMBOL); \
		if(execl("/usr/bin/addr2line", "/usr/bin/addr2line", ptr, "-e", symbol, NULL) == -1) \
			exit(EXIT_FAILURE); \
	} else if(child != -1) {\
		close(pipefd[PIPE_WRITE_END]); \
		waitpid(child, NULL, 0); \
		char line_info[128]; \
		int chars_read = ::read(pipefd[0], line_info, 128); \
		close(pipefd[PIPE_READ_END]); \
		line_info[chars_read-1] = 0; \
		if(strncmp(line_info, "??:0", 4) != 0) { \
			std::cerr << "\t\t" << line_info << std::endl; \
		} \
	} \
}

#define _PRINT_STACK \
{ \
	void *buffer[BT_SIZE]; \
	size_t size = backtrace(buffer, BT_SIZE); \
	char** symbols = backtrace_symbols(buffer, size); \
	if(symbols != NULL) { \
		std::cerr << "Stack Trace: " << std::endl; \
		for(int i = 0; i < size; i++) { \
			std::cerr << "\t" << symbols[i] << std::endl; \
			_PRINT_LINE(buffer[i], symbols[i]); \
		} \
		free(symbols); \
	} \
}

#define PRINT_ERRNO(ERRNO) \
{ \
	std::lock_guard<std::mutex> print_lock(xErrorPrintMutex); \
	std::cerr << "Error in thread: " << std::this_thread::get_id() << std::endl; \
	std::cerr << "Errno(" << std::dec << ERRNO << "): " << strerror(ERRNO) << std::endl; \
	_PRINT_STACK; \
}

#define PRINT_ERROR { PRINT_ERRNO(errno); }

#define PRINT_SYSTEM_ERROR(SYSERR) \
{ \
	std::lock_guard<std::mutex> print_lock(xErrorPrintMutex); \
	std::cerr << "Exception: " << SYSERR.what() << std::endl; \
	std::cerr << "Code: " << SYSERR.code().value() << std::endl; \
	std::cerr << "Category: " << SYSERR.code().category().name() << std::endl; \
	std::cerr << "Message: " << SYSERR.code().message() << std::endl; \
	std::cerr << "Errno(" << std::dec << errno << "): " << strerror(errno) << std::endl; \
	_PRINT_STACK; \
}

#define PRINT_ERRNO_AND_THROW(ERRNO) \
{ \
	PRINT_ERRNO(ERRNO); \
	throw std::system_error(std::make_error_code(static_cast<std::errc>(ERRNO))); \
}
#define PRINT_SYSTEM_ERROR_AND_THROW(SYSERR) \
{ \
	PRINT_SYSTEM_ERROR(SYSERR); \
	throw SYSERR; \
}
#define PRINT_ERRNO_AND_EXIT { PRINT_ERRNO; std::exit(EXIT_FAILURE); }
#define PRINT_SYSTEM_ERROR_AND_EXIT(SYSERR) { PRINT_SYSTEM_ERROR(SYSERR); std::exit(EXIT_FAILURE); }

#endif // _EXCEPTION_H
