#ifndef _ROSCOOPENFLOWMESSAGE_H
#define _ROSCOOPENFLOWMESSAGE_H

#include <stdint.h>
#include <vector>
#include <memory>

#include "RoscoTypes.hpp"
#include "OpenFlowMessage.hpp"

#define ROSCO_MSG_BLS      0xAA
#define ROSCO_MSG_BLS_AGG  0xAB
#define ROSCO_MSG_BLS_LDR  0xAC
#define ROSCO_MSG_BLS_AGG_ROLE 0xAD

struct RoscoOpenFlowBLSShareMessageBody {
	struct OpenFlowHeader xHeader;
	uint8_t xNodeId;
	uint8_t xSignatureLength;
	// char[] xSignature;
	// char[] xMessage;
}__attribute__((packed));

class RoscoOpenFlowBLSShareMessage : public OpenFlowMessage
{
	public:
		RoscoOpenFlowBLSShareMessage(OpenFlowMessage& other);
		OpenFlowMessagePtr getMessage();

		uint8_t getNodeId() const { return ((struct RoscoOpenFlowBLSShareMessageBody*)xData->data())->xNodeId; }
		const CharVectorPtr getSignature() const { return std::make_shared<CharVector>(xSignaturePtr, xSignaturePtr+(xSignatureLength)); }
		const CharVectorPtr getMessageBytes() const { return std::make_shared<CharVector>(xMessagePtr, xMessagePtr+(xMessageLength)); }

	private:
		uint8_t xSignatureLength;
		char* xSignaturePtr;
		uint8_t xMessageLength;
		char* xMessagePtr;
};

struct RoscoOpenFlowBLSAggMessageBody {
	struct OpenFlowHeader xHeader;
	uint8_t xSignatureLength;
	// char[] xSignature;
	// char[] xMessage;
}__attribute__((packed));

class RoscoOpenFlowBLSAggMessage : public OpenFlowMessage
{
	public:
		RoscoOpenFlowBLSAggMessage(OpenFlowMessage& other);
		OpenFlowMessagePtr getMessage();

		const CharVectorPtr getSignature() const { return std::make_shared<CharVector>(xSignaturePtr, xSignaturePtr+(xSignatureLength)); }
		const CharVectorPtr getMessageBytes() const { return std::make_shared<CharVector>(xMessagePtr, xMessagePtr+(xMessageLength)); }

	private:
		uint8_t xSignatureLength;
		char* xSignaturePtr;
		uint8_t xMessageLength;
		char* xMessagePtr;
};

#endif // _ROSCOOPENFLOWMESSAGE_H
