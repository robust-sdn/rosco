#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <system_error>
#include <string>
#include <utility>

#include "util.hpp"
#include "exception.hpp"
#include "Logger.hpp"
#include "OpenFlowMessageReader.hpp"
#include "OpenFlowConnection.hpp"

#define MAX_BACKLOG 10

OpenFlowConnection::OpenFlowConnection() : 
	xRunning(false), xConnected(false), xSocketFd(-1),
	xCallback(NULL), xProtocolVersion(0) {}

OpenFlowConnection::OpenFlowConnection(int fd) :
	xRunning(false), xConnected(true), xSocketFd(fd),
	xWriter(std::make_unique<OpenFlowMessageWriter>(fd)), xReader(std::make_unique<OpenFlowMessageReader>(fd)),
	xCallback(NULL)
{
	setConnectionName();
}
		
void OpenFlowConnection::setConnectionName()
{
	if(xSocketFd < 0) {
		return;
	}
	struct sockaddr_storage peer_addr;
	socklen_t peer_addr_len = sizeof(peer_addr);

	if(getpeername(xSocketFd, (struct sockaddr*)&peer_addr, &peer_addr_len) == -1) {
		PRINT_ERRNO_AND_THROW(errno);
	}
	char host[NI_MAXHOST], service[NI_MAXSERV];
	int rv;
	if((rv = getnameinfo((struct sockaddr *) &peer_addr, peer_addr_len, host, NI_MAXHOST, service, NI_MAXSERV, NI_NUMERICSERV)) != 0) {
		throw std::system_error(rv, std::generic_category(), gai_strerror(rv));
	}
	xPeerName = host;
	if(strlen(service) > 0) {
		xPeerName += ":";
		xPeerName += service;
	}
}

OpenFlowConnection::~OpenFlowConnection()
{
	if(xRunning) {
		stop();
		waitDone();
	}
}

void OpenFlowConnection::connect(const std::string& host, const std::string& port)
{
	if(xRunning) {
		stop();
		waitDone();
	}
	if(xConnected) {
		closeConnection();
	}

	struct addrinfo hints;
	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	hints.ai_protocol = 0;

	int rv;
	struct addrinfo* res;
	if((rv = getaddrinfo(host.c_str(), port.c_str(), &hints, &res)) != 0) {
		PRINT_SYSTEM_ERROR_AND_THROW(std::system_error(rv, std::generic_category(), gai_strerror(rv)));
	}

	int connect_errno;
	struct addrinfo* rp;
	for(rp = res; rp != NULL; rp = rp->ai_next) {
		if((xSocketFd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol)) == -1) {
			continue;
		}
		if (::connect(xSocketFd, rp->ai_addr, rp->ai_addrlen) != -1) {
			break;
		}
		connect_errno = errno;
		close(xSocketFd);
	}
	if(rp == NULL) {
		xSocketFd = -1;
		freeaddrinfo(res);
		PRINT_SYSTEM_ERROR_AND_THROW(std::system_error(connect_errno, std::generic_category(), "Could Not Connect"));
	}
	xWriter.reset(new OpenFlowMessageWriter(xSocketFd));
	xReader.reset(new OpenFlowMessageReader(xSocketFd));
	setConnectionName();
	freeaddrinfo(res);

	xConnected = true;
}

void OpenFlowConnection::waitDone()
{
	if(xConnectionThread->joinable() && std::this_thread::get_id() != xConnectionThread->get_id()) {
		xConnectionThread->join();
	}
}

void OpenFlowConnection::closeConnection()
{
	if(xConnected) {
		Logger{} << "DISCONNECTING FROM: " << xPeerName << std::endl;
		xConnected = false;
		shutdown(xSocketFd, SHUT_RDWR);
	}
}

void OpenFlowConnection::sendMessage(const OpenFlowMessage& msg)
{
	if(!xConnected) {
		PRINT_ERRNO_AND_THROW(ENOTCONN);
	}
	if(xWriter->write(msg) == false) {
		PRINT_ERRNO_AND_THROW(ECONNABORTED);
	}
}

void OpenFlowConnection::sendBytes(const void* buf, size_t size)
{
	if(!xConnected) {
		PRINT_ERRNO_AND_THROW(ENOTCONN);
	}
	if(xWriter->writeBytes(buf, size) == false) {
		PRINT_ERRNO_AND_THROW(ECONNABORTED);
	}
}

void OpenFlowConnection::start(OpenFlowConnectionCallback* callback)
{
	std::lock_guard<std::mutex> lock(xConnectionMutex);
	if(xRunning) {
		return;
	}
	if(!xConnected) {
		PRINT_ERRNO_AND_THROW(ENOTCONN);
	}
	xCallback = callback;
	xRunning = true;
	xConnectionThread = std::make_unique<std::thread>(&OpenFlowConnection::runLoop, this);
}

void OpenFlowConnection::stop()
{
	std::lock_guard<std::mutex> lock(xConnectionMutex);
	if(xRunning) {
		xRunning = false;
		closeConnection();
	}
}

void OpenFlowConnection::runLoop()
{
	while(xRunning) {
		OpenFlowMessagePtr input_message;
		input_message = xReader->read();
		if(!xRunning) {
			break;
		}
		if(input_message == nullptr) {
			stop();
		} else if(input_message->getType() == OF_MSG_HELLO) {
			xProtocolVersion = input_message->getVersion();
		}
		if(xCallback) {
			xCallback->callback(this, input_message);
		}
	}
	close(xSocketFd);
}
