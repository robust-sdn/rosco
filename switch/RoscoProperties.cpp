#include <string>
#include <algorithm>
#include <functional>
#include <locale>
#include <unordered_map>
#include <utility>
#include <fstream>
#include <sstream>

#include "exception.hpp"
#include "RoscoProperties.hpp"

static inline void ltrim(std::string& s)
{
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
}

static inline void rtrim(std::string& s)
{
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

static inline void trim(std::string& s)
{
	ltrim(s);
	rtrim(s);
}

RoscoProperties::RoscoProperties(const char* file_path, int id)
{
	std::ifstream prop_file;

	try {
		prop_file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
		prop_file.open(file_path);
		prop_file.exceptions(std::ifstream::goodbit);
	} catch(const std::system_error& e) {
		PRINT_SYSTEM_ERROR_AND_EXIT(e);
	}

	std::string line;
	while(getline(prop_file, line)) {
		ltrim(line);
		if(line.length() == 0) {
			continue;
		}
		if(line[0] == '#') {
			continue;
		}
		rtrim(line);
		size_t found = line.find_first_of("=");
		if(found == std::string::npos) {
			continue;
		}
		std::string key = line.substr(0, found);
		std::string value = line.substr(found + 1);
		xPropertiesMap.insert(std::make_pair(key, value));
	}
	prop_file.close();
}

const std::string& RoscoProperties::getProperty(const std::string& key) const
{
	std::unordered_map<std::string,std::string>::const_iterator value = xPropertiesMap.find(key);
	if(value == xPropertiesMap.end()) {
		std::stringstream msg_stream;
		msg_stream << "Property " << key << " not found";
		throw std::system_error(ENODEV, std::generic_category(), msg_stream.str());
	}
	return value->second;
}
