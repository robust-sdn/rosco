#ifndef _ROSCO_PROPERTIES_H
#define _ROSCO_PROPERTIES_H

#include <unordered_map>
#include <string>

class RoscoProperties
{
	public:
		RoscoProperties(const char* file, int id);
		const std::string& getProperty(const std::string& key) const;

	private:
		std::unordered_map<std::string, std::string> xPropertiesMap;
};

#endif // _ROSCO_PROPEREIS_H
