#include <iostream>

#include "OpenFlowConnection.hpp"
#include "OpenFlowConnectionProxy.hpp"

OpenFlowConnectionProxy::~OpenFlowConnectionProxy()
{
	closeConnection();
}

void OpenFlowConnectionProxy::openConnection()
{
	xControllerConnection->start(this);
	xSwitchConnection->start(this);
}

void OpenFlowConnectionProxy::closeConnection()
{
	xControllerConnection->stop();
	xControllerConnection->waitDone();
	xSwitchConnection->stop();
	xSwitchConnection->waitDone();
}

uint8_t OpenFlowConnectionProxy::getNegotiatedVersion() const
{
	uint8_t controller_version = xControllerConnection->getVersion();
	uint8_t switch_version = xSwitchConnection->getVersion();
	return controller_version > switch_version ? switch_version : controller_version;
}

void OpenFlowConnectionProxy::sendSwitch(const OpenFlowMessage& msg)
{
	xSwitchConnection->sendMessage(msg);
}

void OpenFlowConnectionProxy::sendSwitch(const void* buf, size_t size)
{
	xSwitchConnection->sendBytes(buf, size);
}

void OpenFlowConnectionProxy::sendController(const OpenFlowMessage& msg)
{
	xControllerConnection->sendMessage(msg);
}

void OpenFlowConnectionProxy::sendController(const void* buf, size_t size)
{
	xControllerConnection->sendBytes(buf, size);
}

void OpenFlowConnectionProxy::callback(OpenFlowConnection* con, OpenFlowMessagePtr& ptr)
{
	if(ptr == nullptr) {
		closeConnection();
		return;
	}
	if(con == xControllerConnection.get()) {
		xMessageHandler.handleMessageFromController(*this, *ptr);
		return;
	}
	if(con == xSwitchConnection.get()) {
		xMessageHandler.handleMessageFromSwitch(*this, *ptr);
		return;
	}
}
