#!/usr/bin/perl

use strict;

use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

use constant {
	CRITICAL => 50,
	ERROR => 40,
	WARNING => 30,
	INFO => 20,
	DEBUG => 10,
	NOTSET => 0,
};

my $log_dir = "/tmp/rosco/log";
my $log_level = CRITICAL;
my $port = 4000;

my $script = shift;
$script = "$script_dir/$script" if(defined $script);
print "WARNING: No RYU script specified\n" unless(defined $script);

my $startrosco = "$proj_dir/java/runscripts/runrosco.pl -f RoscoController";
my $startryu = "$script_dir/startryuctl";

my $controller_nodes = get_lines_from_file("$proj_dir/java/config/controllernodelist");
for(0 .. $#{$controller_nodes}) {
	my $controller = $controller_nodes->[$_];
	printf "Starting controller %s %d\n", $controller, $_;
	system("ssh -oStrictHostKeyChecking=no $controller mkdir -p $log_dir");
	system("ssh -oStrictHostKeyChecking=no $controller $startrosco $_ &");
	if(defined($script)) {
		sleep 1;
		system("ssh -oStrictHostKeyChecking=no $controller $startryu $_ $script $log_dir $port $log_level");
	}
}

sub print_usage {
	my $file_name = basename($0);
	die "Start all Ryu Controllers\nUsage: $file_name [controller_app]\n";
}
