#!/usr/bin/perl

use strict;

use IPC::Open3;
use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/common.pl";

$| = 1;

my $controller_script = "$script_dir/topo_learner_13.py";
#my $controller_script = "topo_learner_13.py";

for(0 .. 100) {
	run_ryu_discovery();
	sleep 1;
}

sub run_ryu_discovery {
	end_ryu();
	print `ryu-manager --default-log-level=50 $controller_script`;
}

sub stop_switches {
	system_or_die("$script_dir/runswitches.pl stopovs");
}

sub stop_controllers {
	system_or_die("$script_dir/stopcontrollers.pl");
}

sub startcontrollers {
	system_or_die("$script_dir/startroscocontrollers.pl $controller_script");
}

sub startswitches {
	system_or_die("$script_dir/runswitches.pl startovs");
}

=pub
my $alarm_time = 30;
sub run_ryu_discovery {
	end_ryu();
	my $pid = open3(\*CHLD_IN, \*CHLD_OUT, \*CHLD_ERR, "ryu-manager $controller_script");
	my $time = 0;
	eval {
		local $SIG{ALRM} = sub { print "$time\n"; end_ryu(); };
		alarm $alarm_time;
		while(<CHLD_OUT>) {
			print "||$_";
			chomp;
			$time = $_;
		}
	};
	if ($@) {
		die unless $@ eq "alarm\n";
	}
}
=cut
	
sub end_ryu {
	for(split(/\n/, `ps aux | grep ryu-manager | grep -v grep`)) {
		if(/^\w+\s+(\d+)/) {
			system("kill $1");
		}
	}
}
