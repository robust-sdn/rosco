#!/usr/bin/perl

use strict;

use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
my $ip_tmpl = "$script_dir/interfaces.tmpl";
my $subnet = "172.16";

my $ip = shift or print_usage();
print_usage() unless($ip =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/);

my $iface = find_interface_on_subnet("172.16");
die "Could not find iface on subnet $subnet.0.0\n" unless defined($iface);

print get_new_interfaces_file($ip, $iface);

sub get_new_interfaces_file {
	my $ip = shift;
	my $iface = shift;
	my $tmpl = get_template_file();
	$tmpl =~ s/%%IFACE%%/$iface/g;
	$tmpl =~ s/%%IP%%/$ip/g;
	return $tmpl;
}

sub get_template_file {
	open TMPL, "<", $ip_tmpl or die "Unable to open $ip_tmpl: $!\n";
	my $template_content = do { local $/; <TMPL> };
	close TMPL;
	return $template_content;
}

sub find_interface_on_subnet {
	my $subnet = shift;
	my $iface;
	for(split(/\n/, `ifconfig -a`)) {
		if(/^([a-z0-9]+)\s/) {
			$iface = $1;
		}
		if(/^\s*inet addr:(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s/) {
			my $ip = $1;
			if($ip =~ /^$subnet/) {
				return $iface;
			}
		}
	}
	return undef;
}

sub print_usage {
        my $file_name = basename($0);
        die "Usage: $file_name ip_address\n";
}
