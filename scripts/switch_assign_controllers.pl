#!/usr/bin/perl

use strict;
use Data::Dumper;
use File::Basename;
use Cwd 'abs_path';

my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my $bridge_name = "br0";
my $controller_nodes = "$proj_dir/java/config/controllernodelist";

my $controllerlist =  get_lines_from_file($controller_nodes);
my $controller_ips = getHostIPs($controllerlist);

addControllersToBridge($controller_ips, $bridge_name);

{
	my @added_controllers = ();
	sub addControllersToBridge {
		my ($hosts, $bridge) = @_;
		for(keys %{$hosts}) {
			next if(/-consensusPlane/);
			my $controller = $hosts->{$_};
			unless(grep(/$controller/, @added_controllers)) {
				push @added_controllers, $controller;
			}
		}	
		system_or_die("sudo ovs-vsctl set-controller $bridge " . join(" ", map { "tcp:$_:6633" } @added_controllers));
	}
}
