#!/usr/bin/perl

use strict;

use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
my $src_dir = dirname($script_dir);

my $hosts_file = "/etc/hosts";

use constant {
	STOP_ON_ERROR => 0,
	CONTINUE_ON_ERROR => 1,
	NO_PRINT_COMMAND => 0,
	PRINT_COMMAND => 1,
};

my $print_command = 0;

sub _do_system {
	my $continue_on_error = shift;
	if($print_command == 1) {
		print "@_\n";
	}
	my $result = `@_`;
	unless($? == 0) {
		if($continue_on_error) {
			print STDERR "system @_ failed: $?\n";
		} else {
			die "system @_ failed: $?\n";
		}
	}
	return $result;
}

sub set_system_echo {
	$print_command = 1;
}

sub clear_system_echo {
	$print_command = 0;
}

sub exec_or_die {
	if($print_command == 1) {
		print "@_\n";
	}
	exec(@_);
	die "exec @_ failed: $!\n";
}

sub system_or_die {
	return _do_system(STOP_ON_ERROR, @_);
}

sub system_or_continue {
	return _do_system(CONTINUE_ON_ERROR, @_);
}

sub get_lines_from_file {
	my $file = shift;
	my $lines;
	open(FILE, "<$file" ) or die "Unable to open $file: $!\n";
	while(<FILE>) {
		chomp;
		push @{$lines}, $_;
	}
	return $lines;
}

sub getHostIPs {
	my $hosts = shift;
	my $ips = {};
	my $hosts_file = get_lines_from_file($hosts_file);
	for(@{$hosts}) {
		my $host = $_;
		for(@{$hosts_file}) {
			my $line = $_;
			if($line =~ /([0-9.]+)\s+($host\S*)/) {
				$ips->{$2} = $1;
			}
		}
	}
	return $ips;
}

sub getIFaceIPs {
	my $ifaces = {};
	for(split(/\n\n/, `/sbin/ifconfig -a`)) {
		my $iface = undef;
		my $ip = undef;
		for(split(/\n/, $_)) {
			if(/^(\w+)\s+/) {
				$iface = $1;
			}
			if(/inet addr:([0-9]+\.[0-9]+\.[0-9]+\.+[0-9]+)\s+/) {
				$ip = $1;
			}
		}
		if(defined($iface)) {
			$ifaces->{$iface} = $ip;
		}
	}

	return $ifaces;
}

sub isRyuRunning {
	for(split(/\n/, `ps aux | grep ryu-manager`)) {
		if(/^\w+\s+(\d+).*ryu-manager/) {
			if($1 > 0) {
				return 1;
			}
		}
	}
	return 0;
}

return 1;
