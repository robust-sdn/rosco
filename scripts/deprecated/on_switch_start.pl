#!/usr/bin/perl

use strict;
use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/switch_utils.pl";

for(split(/\n/, `sudo ovs-vsctl list-br`)) {
	my $bridge = $_;
	clear_ip_and_enable_iface($bridge);
	for(split(/\n/, `sudo ovs-vsctl list-ifaces $bridge`)) {
		clear_ip_and_enable_iface($_);
	}
}

sub clear_ip_and_enable_iface {
	my $iface = shift;
	system_or_exit("sudo ip addr flush $iface");
	system_or_exit("sudo ifconfig $iface up");
}
