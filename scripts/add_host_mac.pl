#!/usr/bin/perl

use strict;
use Data::Dumper;
use File::Basename;
use Cwd 'abs_path';

my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my $host_nodes = "$proj_dir/java/config/hostnodelist";
my $hostlist =  get_lines_from_file($host_nodes);
my $host_ips = getHostIPs($hostlist);
my $filtered_ips = filterIPs($host_ips);

for (keys %{$filtered_ips}) {
	my $host = $_;
	my $host_ip = $filtered_ips->{$_};
	my $host_mac = _get_host_mac($host, $host_ip);
	if(defined($host_mac)) {
		print "$host_ip $host_mac\n";
		system_or_continue("sudo arp -d $host_ip");
		system_or_die("sudo arp -s $host_ip $host_mac");
	}
}

sub filterIPs {
	my $ips = shift;
	my $filtered_ips;
	
	my $hostname = `/bin/hostname -s`;
	chomp $hostname;
	
	my $ifaces = getIFaceIPs();
	for(keys %{$ips}) {
		my $host = $_;
		next if($host =~ /-controlPlane/); # ignore control plane IPs
		next if($host =~ /^$hostname/); # ignore local host
		my $ip = $ips->{$_};
		next if(_is_on_local_host($ifaces, $ip));
		$filtered_ips->{$host} = $ip;
	}

	return $filtered_ips;
}

sub _is_on_local_host {
	my $ifaces = shift;
	my $ip = shift;
	for(values %{$ifaces}) {
		if($ip eq $_) {
			return 1;
		}
	}
	return 0;
}

sub _get_host_mac {
	my $host = shift;
	my $ip = shift;
	
	my $target_host = $host;
	if($host =~ /([^-]+)-/) {
		$target_host = $1;
	}
	
	my $results = system_or_die("ssh -oStrictHostKeyChecking=no $target_host ifconfig -a");
	my $mac = undef;
	for(split(/\n/, $results)) {
		if(/HWaddr ([0-9a-fA-F:]+)/) {
			$mac = $1;
		}
		if(/inet addr:$ip /) {
			return $mac if(defined($mac));
		}
	}
	return undef;
}

