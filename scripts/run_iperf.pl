#!/usr/bin/perl

use strict;

use Data::Dumper;
use Math::Random qw(random_uniform random_poisson);
use File::Basename;
use Cwd 'abs_path';

my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my $host = `hostname -s`;
chomp $host;
my $log_file = "$script_dir/iperflog.$host";
my $total_flows = shift;

my $hadoop_params = {
	'in_rack_ratio'=> 0.13,
    'inter_arrival'=> 20,
    #'inter_arrival'=> 2,
    'in_rack_params'=> {
        'packet_size'=> 250,
        'flow_size'=> 100000,
    },
    'out_of_rack_params'=> {
        'packet_size'=> 250,
        'flow_size'=> 500,
    },
};
my $webserver_params = {
    'in_rack_ratio'=> 0.12,
    'inter_arrival'=> 20,
    'in_rack_params'=> {
        'packet_size'=> 175,
        'flow_size'=> 1000,
    },
    'out_of_rack_params'=> {
        'packet_size'=> 175,
        'flow_size'=> 1000,
    },
};

my $host_nodes = "$proj_dir/java/config/hostnodelist";

my $local_ip = _find_local_host_ip();
my $hostlist =  get_lines_from_file($host_nodes);
my $host_ips = getHostIPs($hostlist);
my $lan_ips = getHostLanIPs($hostlist);
my $external_ips = filterIPs($host_ips, $lan_ips);

#system("sudo killall -s 9 iperf");
#system("/bin/bash -c iperf -s -D 2>&1 /dev/null");
#exit;
system("rm $log_file");

#_generate_flows($total_flows, $hadoop_params);
_generate_flows($total_flows, $webserver_params);

sub _generate_iperf_flow {
	my ($target_ip, $delay, $params) = @_;
    my $packet_count = $params->{"flow_size"} / $params->{"packet_size"};
	my $iperf_cmd = sprintf("$script_dir/iperf_wrap %d %d %d %s", $delay, $params->{"flow_size"}, $params->{"packet_size"}, $target_ip->{'local'});
	#print "$iperf_cmd\n";
	system("$iperf_cmd >> $log_file &");
}

sub _generate_flows {
	my ($total_flows, $params) = @_;
    my $inter_arrival = $params->{"inter_arrival"};
    my $in_rack_ratio = $params->{"in_rack_ratio"};
    my $in_rack_params = $params->{"in_rack_params"};
    my $out_of_rack_params = $params->{"out_of_rack_params"};
        
	my @arrival_times = random_poisson($total_flows, $inter_arrival);
	my @flow_distribution = random_uniform($total_flows);
	my $total_delay = 0;
	my $target_idx = 0;
	my @external_host_names = keys %{$external_ips};

	my $i = 0;
	while($i < $total_flows) {
        my $target_ip = $local_ip;
		my $target_params = $in_rack_params;
		if($flow_distribution[$i] > $in_rack_ratio) {
			$target_ip = $external_ips->{$external_host_names[$target_idx]};
			$target_idx++;
			$target_idx = 0 if($target_idx > $#external_host_names);
			$target_params = $out_of_rack_params;
		}
		_generate_iperf_flow($target_ip, $total_delay, $target_params);
		$i++;
        $total_delay += $arrival_times[$_];
	}
}

sub filterIPs {
	my ($ips, $lan_ips) = @_;
	my $filtered_ips = {};
	
	my $ifaces = getIFaceIPs();
	for(keys %{$ips}) {
		my $ip = $ips->{$_};
		next if(_is_on_local_host($ifaces, $ip));
		$filtered_ips->{$_}->{'local'} = $ip;
		$filtered_ips->{$_}->{'lan'} = $lan_ips->{$_};
	}

	return $filtered_ips;
}
	
sub _find_local_host_ip {
	my $hostname = `/bin/hostname -s`;
	chomp $hostname;
	my $host_ips = getHostIPs([$hostname]);
	my $local_ip;
	for(keys %{$host_ips}) {
		if(/^$hostname($|-)/) {
			$local_ip = $host_ips->{$_};
			last;
		}
	}
	die "Unable to find local host network ip on VLAN.\n" unless(defined($local_ip));
	my $lan_ip = getHostLanIPs([$hostname])->{$hostname};
	die "Unable to find local host network LAN ip on VLAN.\n" unless(defined($lan_ip));
	my $retval = {};
	$retval->{'local'} = $local_ip;
	$retval->{'lan'} = $lan_ip;
	return $retval;
}

sub _find_local_domain {
	my $hostname = `/bin/hostname -s`;
	chomp $hostname;
	my $fullhostname = `/bin/hostname`;
	chomp $fullhostname;
	$fullhostname =~ /$hostname(.*)$/;
	return $1;
}

sub _is_on_local_host {
	my $ifaces = shift;
	my $ip = shift;
	for(values %{$ifaces}) {
		if($ip eq $_) {
			return 1;
		}
	}
	return 0;
}

sub getHostLanIPs {
	my $hosts = shift;
	my $domain = _find_local_domain();
	my $ret = {};
	for(@{$hosts}) {
		my $host = $_;
		my $name_found = 0;
		for(split(/\n/, `nslookup $_$domain`)) {
			if(/Name:/) {
				$name_found = 1;
			}
			if($name_found && /Address:\s*([0-9.]+)/) {
				$ret->{$host} = $1;
			}
		}
	}
	return $ret;
}
