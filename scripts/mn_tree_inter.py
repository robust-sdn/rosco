#!/usr/bin/python

import sys
import os

from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, RemoteController
from mininet.cli import CLI
from mininet.log import setLogLevel, info, output
from mininet.topolib import TreeNet
from time import sleep
import re

PORT = 6633

if __name__ == '__main__':
    
    if(len(sys.argv) < 3):
         raise SystemExit('Usage: %s depth fanout controller1 controller2 ...' % os.path.basename(__file__))
    
    setLogLevel('info')

    depth  = int(sys.argv[1])
    fanout = int(sys.argv[2])
    controllers = sys.argv[3:]

    net = TreeNet(depth=depth, fanout=fanout, controller=RemoteController, switch=OVSKernelSwitch, build=False)

    c = [ net.addController('c%s' % ci, controller=RemoteController,ip=controllers[ci-1], port=PORT)
            for ci in range(1, len(controllers) + 1) ]

    net.start()
    CLI(net)

    net.stop()

