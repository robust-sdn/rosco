use strict;

my $bridge_name = "br0";
my $hosts_file = "/etc/hosts";

my @unused_subnets = ('127.0', '192.168');

sub bridgeName {
	return $bridge_name;
}

sub _is_on_host_subnet {
	my $hosts = shift;
	my $host_type = shift;
	my $ip = shift;

	return 0 unless($ip);

	my $ip_subnet = $ip;
	$ip_subnet =~ s/\.[0-9]+$//;

	for(@{$hosts->{$host_type}}) {
		if(begins_with($_, $ip_subnet)) {
			return 1;
		}
	}
	return 0;
}

sub _is_on_unused_subnet {
	my $ip = shift;

	return 0 unless ($ip);

	for(@unused_subnets) {
		if(begins_with($ip, $_)) {
			return 1;
		}
	}

	return 0;
}

sub filterInterfaces {
	my $hosts = shift;
	my $ifaces = shift;

	my $filtered_ifaces;

	for(keys %{$ifaces}) {
		my $iface = $_;
		next if ($iface eq $bridge_name);
		my $ip = $ifaces->{$iface};
		next if _is_on_host_subnet($hosts, 'c', $ip);
		next if _is_on_unused_subnet($ip);
		if(_is_on_host_subnet($hosts, 'h', $ip) ||
			_is_on_host_subnet($hosts, 'c', $ip)) {
			push @{$filtered_ifaces}, $iface;
		}
	}

	return $filtered_ifaces;
}

sub getHostTypes {
	my $file = $hosts_file;

	my $results = {};
	my @types = ('c', 's', 'h');

	open(HOSTSFILE, "<$hosts_file" ) or die "$!\n";
	while(<HOSTSFILE>) {
		my $line = $_;
		for(@types) {
			my $type = $_;
			if($line =~ /([0-9.]+)\s.*$type([0-9]+)$/) {
				unless(defined($results->{$type})) {
					$results->{$type} = [];
				}
				push @{$results->{$type}}, $1;
			}
		}
	}
	close HOSTFILE;

	my $ifaces = {};
	for(split(/\n\n/, `/sbin/ifconfig -a`)) {
		my $iface = undef;
		my $ip = undef;
		for(split(/\n/, $_)) {
			if(/^(\w+)\s+/) {
				$iface = $1;
			}
			if(/inet addr:([0-9]+\.[0-9]+\.[0-9]+\.+[0-9]+)\s+/) {
				$ip = $1;
			}
		}
		if(defined($iface)) {
			$ifaces->{$iface} = $ip;
		}
	}

	return ($results, $ifaces);
}

sub system_or_exit {
	my $cmd = shift;
	if(do_system($cmd) != 0) {
		die "$cmd failed: $?\n";
	}
}

sub do_system {
	my $cmd = shift;
	print("$cmd\n");
	my $retval = system($cmd);
	return $retval >> 8;
	#return 0;
}

sub begins_with {
	return substr($_[0], 0, length($_[1])) eq $_[1];
}

sub iterator_to_ip {
        my $value = shift;
		my $base_ip = shift;
        my @ip = ();

        my $mod_max = 256 - $base_ip;

        for(0 .. 3) {
                $ip[$_] = (($value / ($mod_max**$_)) % $mod_max) + $base_ip;
        }
        return join(".", reverse @ip);
}

return 1;
