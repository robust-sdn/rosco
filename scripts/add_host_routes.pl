#!/usr/bin/perl

use strict;
use Data::Dumper;
use File::Basename;
use Cwd 'abs_path';

my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my $host_nodes = "$proj_dir/java/config/hostnodelist";

my $local_iface = _find_local_host_iface();
die "Unable to find local host network interface on VLAN.\n" unless(defined($local_iface));

my $hostlist =  get_lines_from_file($host_nodes);
my $host_ips = getHostIPs($hostlist);
my $filtered_ips = filterIPs($host_ips);

for(values %{$filtered_ips}) {
	my $ip_subnet = $_;
	$ip_subnet =~ s/\.[0-9]+$//;
	system_or_continue("sudo route add -net $ip_subnet.0 netmask 255.255.255.0 dev $local_iface");
}

=pub
for (keys %{$filtered_ips}) {
	my $host = $_;
	my $host_ip = $filtered_ips->{$_};
	my $host_mac = _get_host_mac($host, $host_ip);
	if(defined($host_mac)) {
		system_or_die("sudo arp -d $host_ip");
		#system_or_die("sudo arp -s $host_ip $host_mac");
	}
}
system_or_die("echo 255 | sudo tee /proc/sys/net/ipv4/tcp_retries1 > /dev/null");
system_or_die("echo 255 | sudo tee /proc/sys/net/ipv4/tcp_retries2 > /dev/null");
system_or_die("echo 255 | sudo tee /proc/sys/net/ipv4/neigh/$local_iface/mcast_solicit > /dev/null");
=cut

sub filterIPs {
	my $ips = shift;
	my $filtered_ips;
	
	my $ifaces = getIFaceIPs();
	for(keys %{$ips}) {
		my $host = $_;
		my $ip = $ips->{$_};
		next if(_is_on_local_host($ifaces, $ip));
		$filtered_ips->{$host} = $ip;
	}

	return $filtered_ips;
}
	
sub _find_local_host_iface {
	my $hostname = `/bin/hostname -s`;
	chomp $hostname;

	my $host_ips = getHostIPs([$hostname]);

	my $local_ip;
	for(keys %{$host_ips}) {
		if(/^$hostname($|-)/) {
			$local_ip = $host_ips->{$_};
			last;
		}
	}

	return unless defined($local_ip);

	my $ifaces = getIFaceIPs();
	for(keys %{$ifaces}) {
		if($ifaces->{$_} eq $local_ip) {
			return $_;
		}
	}

	return undef;
}

sub _is_on_local_host {
	my $ifaces = shift;
	my $ip = shift;
	for(values %{$ifaces}) {
		if($ip eq $_) {
			return 1;
		}
	}
	return 0;
}

sub _get_host_mac {
	my $host = shift;
	my $ip = shift;
	my $results = system_or_die("ssh -oStrictHostKeyChecking=no $host ifconfig -a");
	my $mac = undef;
	for(split(/\n/, $results)) {
		if(/HWaddr ([0-9a-fA-F:]+)/) {
			$mac = $1;
		}
		if(/inet addr:$ip /) {
			return $mac if(defined($mac));
		}
	}
	return undef;
}

