# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller import dpset
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.ofproto import ether
from ryu.lib import hub
from ryu.lib.packet import arp
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types

import threading
import networkx as nx
import signal
import time
import os
import sys

ROSCO_ARP_DISCOVERY = 10
ROSCO_MAX_XID = 0x7fffffff
ofproto_v1_3.MAX_XID = ROSCO_MAX_XID

DISCOVERY_SLEEP=0.9
DISCOVERY_WAIT=0.05
PATH_SET_SLEEP=0.05
INTER_SLEEP=4

class ShortestPathLearner(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = {
        'dpset': dpset.DPSet,
    }

    def __init__(self, *args, **kwargs):
        super(ShortestPathLearner, self).__init__(*args, **kwargs)
        self.datapaths = {}
        self.host_macs = set()
        self.dpset = kwargs['dpset']
        self.net=nx.DiGraph()
        self.sentflowmods = {}
        self.start_time = 0
        self.is_discovery_active = False
        self.discovery_thread = None
        signal.signal(signal.SIGINT, self.sig_stop)
        self.discovery_lock = threading.Lock()
        self.poll_event = hub.Event()
   
    def sig_stop(self, signum, frame):
        self.stop_discovery()
        os._exit(0)
        #sys.exit()

    def start_discovery(self):
        self.is_discovery_active = True
        self.discovery_thread = hub.spawn(self.poll_loop)

    def stop_discovery(self):
        if(self.is_discovery_active):
            self.is_discovery_active = False
            self.poll_event.set()
            self.discovery_thread.wait()

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions, force=True)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None, force=False):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)

        self.sentflowmods.setdefault(datapath.id, [])

        if(force):
            datapath.send_msg(mod)
            return True

        mod_str = mod.__str__()
        if(mod_str not in self.sentflowmods[datapath.id]):
            datapath.send_msg(mod)
            self.sentflowmods[datapath.id].append(mod_str)
            return True
        return False
        
        #datapath.send_msg(mod)
        #return True

    def _mac_to_datapath_port(self, mac):
        for datapath in self.datapaths.values():
            for p in self.dpset.get_ports(datapath.id):
                if(p.hw_addr == mac):
                    return [datapath, p]
        raise KeyError

    def _port_to_datapath_port(self, dp, port_no):
        datapath = self.datapaths[dp.id]
        if datapath:
            for p in self.dpset.get_ports(datapath.id):
                if(p.port_no == port_no):
                    return [datapath, p]
        raise KeyError


    def check_topology_learned(self):
        for src_mac in self.host_macs:
            for tgt_mac in self.host_macs:
                if(src_mac == tgt_mac):
                    continue
                try:
                    path = nx.shortest_path(self.net, src_mac, tgt_mac)
                except:
                    return False
        return True

    def print_flow_mods(self):
        for src_mac in self.host_macs:
            for tgt_mac in self.host_macs:
                if(src_mac == tgt_mac):
                    continue
                try:
                    path = nx.shortest_path(self.net, src_mac, tgt_mac)
                except:
                    continue
                try:
                    for step in path:
                        if(step == src_mac):
                            continue
                        if(step == tgt_mac):
                            break;
                        next_step = path[path.index(step)+1]
                        print "%d %s %d" % (self.datapaths[step].id, tgt_mac, self.net[step][next_step]['port'].port_no)
                except:
                    pass

    def apply_all_pairs_flows(self):
        updated_datapaths = {}
        for src_mac in self.host_macs:
            for tgt_mac in self.host_macs:
                if(src_mac == tgt_mac):
                    continue
                try:
                    path = nx.shortest_path(self.net, src_mac, tgt_mac)
                except:
                    continue
                try:
                    for step in path:
                        if(step == src_mac):
                            continue
                        if(step == tgt_mac):
                            break;
                        next_step = path[path.index(step)+1]
                        if(self.add_flow(
                            self.datapaths[step], 1,
                            parser.OFPMatch(eth_dst=tgt_mac),
                            [parser.OFPActionOutput(self.net[step][next_step]['port'].port_no)])):
                            updated_datapaths[step] = 1;
                except:
                    pass
        for dpid in updated_datapaths:
            self.datapaths[dpid].send_barrier()

    def handle_arp_discovery(self, datapath, eth, arp, in_port):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        try:
            [src, src_port] = self._mac_to_datapath_port(eth.src)
            [dst, dst_port] = self._port_to_datapath_port(datapath, in_port)
            #[dst, dst_port] = self._mac_to_datapath_port(eth.src)
            #[src, src_port] = self._port_to_datapath_port(datapath, in_port)
        except:
            return
       
        self.discovery_lock.acquire()

        #print "DISCOVERY SRC-%s:%s:%s TGT-%s:%s:%s" % (src.id, src_port.port_no, src_port.hw_addr, dst.id, dst_port.port_no, dst_port.hw_addr)
        if(not self.net.has_edge(src.id, dst.id)):
            self.net.add_edge(src.id, dst.id, { 'port': src_port })
        
        if(self.check_topology_learned()):
            print time.time() - self.start_time
            sys.stdout.flush()
            self.sig_stop(0, 0)

        self.discovery_lock.release()

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
        
        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src

        dpid = datapath.id

        if(eth.ethertype == ether_types.ETH_TYPE_ARP):
            arppkt = pkt.get_protocols(arp.arp)[0]
            #if(arppkt.opcode == arp.ARP_REQUEST):
            if(arppkt.opcode == ROSCO_ARP_DISCOVERY):
                self.handle_arp_discovery(datapath, eth, arppkt, in_port)
                return
            if(arppkt.opcode == arp.ARP_REPLY):
                return

    def _send_garp(self, datapath):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        for port in self.dpset.get_ports(datapath.id):
            if(not port.state == ofproto.OFPPS_LIVE):
                continue
            actions = [parser.OFPActionOutput(port.port_no)]

            e = ethernet.ethernet(ethertype=ether.ETH_TYPE_ARP, src=port.hw_addr)
            #a = arp.arp(opcode=arp.ARP_REQUEST, src_mac=port.hw_addr)
            a = arp.arp(opcode=ROSCO_ARP_DISCOVERY, src_mac=port.hw_addr)
            p = packet.Packet()
            p.add_protocol(e)
            p.add_protocol(a)
            p.serialize()

            #print "GARP:", datapath.id, port.hw_addr, e.dst

            datapath.send_msg(parser.OFPPacketOut(datapath=datapath, buffer_id=ofproto.OFP_NO_BUFFER,
                in_port=datapath.ofproto.OFPP_CONTROLLER, actions=actions, data=p.data))
            hub.sleep(DISCOVERY_WAIT)

    def poll_loop(self):
        hub.sleep(INTER_SLEEP)
        if(self.start_time == 0):
            self.start_time = time.time()
        #while self.is_discovery_active:
        for datapath in self.datapaths.values():
            self._send_garp(datapath)
        #self.poll_event.wait(timeout=DISCOVERY_SLEEP)

    def _handle_switch_enter(self, ev):
        self.net.add_node(ev.dp.id)
        for port in self.dpset.get_ports(ev.dp.id):
            if(not port.state == ev.dp.ofproto.OFPPS_LIVE):
                continue
            self.net.add_node(port.hw_addr)
            self.host_macs.add(port.hw_addr)
            self.net.add_edge(port.hw_addr, ev.dp.id)
            self.net.add_edge(ev.dp.id, port.hw_addr)
        self.datapaths[ev.dp.id] = ev.dp
        if(self.is_discovery_active is False):
            self.start_discovery()

    def _handle_switch_exit(self, ev):
        del self.datapaths[ev.dp.id]
        del self.sentflowmods[ev.dp.id]
    
    @set_ev_cls(dpset.EventDP)
    def _dp_handler(self, ev):
        if(ev.enter):
            self._handle_switch_enter(ev)
        else:
            self._handle_switch_exit(ev)

    @set_ev_cls(ofp_event.EventOFPBarrierReply, MAIN_DISPATCHER)
    def _handle_barrier_reply(self, ev):
        if(self.start_time != 0):
            print time.time() - self.start_time
