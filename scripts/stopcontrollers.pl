#!/usr/bin/perl

use strict;

use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);
require "$script_dir/common.pl";

my $stoprosco = "$proj_dir/java/runscripts/stoprosco.pl";
my $stopryu = "$script_dir/stopryuctl";

my $controller_nodes = get_lines_from_file("$proj_dir/java/config/controllernodelist");
for(0 .. $#{$controller_nodes}) {
	my $controller = $controller_nodes->[$_];
	printf "Stopping controller %s %d\n", $controller, $_;
	system("ssh -oStrictHostKeyChecking=no $controller $stoprosco $_ RoscoController &");
	system("ssh -oStrictHostKeyChecking=no $controller $stopryu $_");
}
