#!/usr/bin/perl

use strict;

use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/common.pl";

my $script = shift;
$script = "$script_dir/$script";

while(<>) {
	chomp;
	print "Running $script on $_\n";
	system("ssh -oStrictHostKeyChecking=no $_ $script");
}
