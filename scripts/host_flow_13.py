# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller import dpset
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.ofproto import ether
from ryu.lib import hub
from ryu.lib.packet import arp
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types

import random
import threading
import networkx as nx
import signal
import time
import sys

ROSCO_ARP_DISCOVERY = 10
ROSCO_MAX_XID = 0x7fffffff
ofproto_v1_3.MAX_XID = ROSCO_MAX_XID

DISCOVERY_SLEEP=0.9
DISCOVERY_WAIT=0.05
PATH_SET_SLEEP=0.05

INTER_SLEEP=15
FLOW_COUNT=8000

SRC='00:04:23:ae:cc:7e'
TGT='00:04:23:c5:e1:4f'

class HostArrivalLearner(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = {
        'dpset': dpset.DPSet,
    }

    def __init__(self, *args, **kwargs):
        super(HostArrivalLearner, self).__init__(*args, **kwargs)
        self.datapaths = {}
        self.host_macs = set()
        self.dpset = kwargs['dpset']
        self.net=nx.DiGraph()
        self.current_count = 0
        self.start_time = 0
        self.apply_path = None
        self.apply_step = -1
        self.apply_target = None
        signal.signal(signal.SIGINT, self.sig_stop)
        self.discovery_lock = threading.Lock()
        self.poll_event = hub.Event()
        self.apply_event = hub.Event()
        self.is_discovery_active = False
        self.is_flow_apply_active = True
        self.threads.append(hub.spawn(self.apply_path_loop))

    def sig_stop(self, signum, frame):
        self.stop_discovery()
        self.stop_flow_apply()
        sys.exit()

    def start_discovery(self):
        self.is_discovery_active = True
        self.threads.append(hub.spawn(self.poll_loop))

    def stop_discovery(self):
        self.is_discovery_active = False
        self.poll_event.set()

    def stop_flow_apply(self):
        self.is_flow_apply_active = False
        self.apply_event.set()

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        # install table-miss flow entry
        #
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.  The bug has been fixed in OVS v2.1.0.
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions, force=True)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None, force=False):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    def _mac_to_datapath_port(self, mac):
        for datapath in self.datapaths.values():
            for p in self.dpset.get_ports(datapath.id):
                if(p.hw_addr == mac):
                    return [datapath, p]
        raise KeyError

    def _port_to_datapath_port(self, dp, port_no):
        datapath = self.datapaths[dp.id]
        if datapath:
            for p in self.dpset.get_ports(datapath.id):
                if(p.port_no == port_no):
                    return [datapath, p]
        raise KeyError

    def _clear_apply_path(self):
        self.apply_path = None
        self.apply_step = -1
        self.apply_target = None

    def _send_next_path_update(self):
        step = self.apply_path[self.apply_step]
        next_step = self.apply_path[self.apply_step+1]
        port = self.net[step][next_step]['port']
        datapath = self.datapaths[step]
        parser = datapath.ofproto_parser
        self.add_flow(datapath, 1, parser.OFPMatch(eth_dst=self.apply_target), [parser.OFPActionOutput(port.port_no)])
        datapath.send_barrier()

    def set_flow_path(self, src, tgt):
        if(src == tgt):
            return False
        if(self.apply_path is not None):
            self.logger.debug("Update in progress")
            return False
        try:
            self.apply_path = nx.shortest_path(self.net, src, tgt)
        except:
            self.logger.debug("No known path from %s to %s", src, tgt)
            self._clear_apply_path()
            return False

        if(len(self.apply_path) < 4):
            self.logger.debug("No update for path needed %s", self.apply_path)
            self._clear_apply_path()
            return False
      
        if(self.is_discovery_active):
            self.stop_discovery()
            hub.sleep(INTER_SLEEP)

        self.start_time = time.time()
        self.apply_target = tgt
        self.apply_step = len(self.apply_path) - 3
        self._send_next_path_update()
        return True

    def handle_arp_discovery(self, datapath, eth, arp, in_port):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        try:
            [src, src_port] = self._mac_to_datapath_port(eth.src)
            [dst, dst_port] = self._port_to_datapath_port(datapath, in_port)
        except:
            return
       
        self.discovery_lock.acquire()

        if(self.net.has_edge(src.id, dst.id)):
            self.discovery_lock.release()
            return

        #self.logger.info("DISCOVERY SRC-%s:%s:%s TGT-%s:%s:%s", src.id, src_port.port_no, src_port.hw_addr, dst.id, dst_port.port_no, dst_port.hw_addr)

        self.net.add_edge(src.id, dst.id, { 'port': src_port })
        self.discovery_lock.release()

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]
        
        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src

        dpid = datapath.id

        if(eth.ethertype == ether_types.ETH_TYPE_ARP):
            arppkt = pkt.get_protocols(arp.arp)[0]
            #if(arppkt.opcode == arp.ARP_REQUEST):
            if(arppkt.opcode == ROSCO_ARP_DISCOVERY):
                self.handle_arp_discovery(datapath, eth, arppkt, in_port)
                return
            if(arppkt.opcode == arp.ARP_REPLY):
                return

    def _send_garp(self, datapath):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        for port in self.dpset.get_ports(datapath.id):
            if(not port.state == ofproto.OFPPS_LIVE):
                continue
            actions = [parser.OFPActionOutput(port.port_no)]

            e = ethernet.ethernet(ethertype=ether.ETH_TYPE_ARP, src=port.hw_addr)
            #a = arp.arp(opcode=arp.ARP_REQUEST, src_mac=port.hw_addr)
            a = arp.arp(opcode=ROSCO_ARP_DISCOVERY, src_mac=port.hw_addr)
            p = packet.Packet()
            p.add_protocol(e)
            p.add_protocol(a)
            p.serialize()

            datapath.send_msg(parser.OFPPacketOut(datapath=datapath, buffer_id=ofproto.OFP_NO_BUFFER,
                in_port=datapath.ofproto.OFPP_CONTROLLER, actions=actions, data=p.data))
            hub.sleep(DISCOVERY_WAIT)

    def poll_loop(self):
        hub.sleep(INTER_SLEEP)
        while self.is_discovery_active:
            for datapath in self.datapaths.values():
                self._send_garp(datapath)
            self.poll_event.wait(timeout=DISCOVERY_SLEEP)

    def apply_path_loop(self):
        while(self.is_flow_apply_active):
            self.apply_event.wait(timeout=PATH_SET_SLEEP)
            if(len(self.host_macs) > 1):
                #src = random.sample(self.host_macs, 1)[0]
                #tgt = random.sample(self.host_macs, 1)[0]
                #self.set_flow_path(src, tgt)
                self.set_flow_path(SRC, TGT)


    def _handle_switch_enter(self, ev):
        self.net.add_node(ev.dp.id)
        for port in self.dpset.get_ports(ev.dp.id):
            if(not port.state == ev.dp.ofproto.OFPPS_LIVE):
                continue
            self.net.add_node(port.hw_addr)
            self.host_macs.add(port.hw_addr)
            self.net.add_edge(port.hw_addr, ev.dp.id)
            self.net.add_edge(ev.dp.id, port.hw_addr)
        self.datapaths[ev.dp.id] = ev.dp
        if(self.is_discovery_active is False):
            self.start_discovery()

    def _handle_switch_exit(self, ev):
        del self.datapaths[ev.dp.id]
    
    @set_ev_cls(dpset.EventDP)
    def _dp_handler(self, ev):
        if(ev.enter):
            self._handle_switch_enter(ev)
        else:
            self._handle_switch_exit(ev)

    @set_ev_cls(ofp_event.EventOFPBarrierReply, MAIN_DISPATCHER)
    def _handle_barrier_reply(self, ev):
        if(self.apply_path is not None):
            self.apply_step -= 1
        if(self.apply_step > 0):
            self._send_next_path_update()
        else:
            #self.logger.critical("%d %f", len(self.apply_path), time.time() - self.start_time)
            print"%d %f" % (len(self.apply_path), time.time() - self.start_time)
            sys.stdout.flush()
            self._clear_apply_path()
            self.current_count += 1
            if(self.current_count >= FLOW_COUNT):
                self.sig_stop(0, 0)
