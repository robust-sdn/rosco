#!/usr/bin/perl

use strict;

use Data::Dumper;

my @results;
for(@ARGV) {
	open FILE, "<", $_ or die "Unable to open $_: $!\n";
	my $start_time = 0;
	while(<FILE>) {
		chomp;
		my @row = split(/,/, $_);
		$start_time = $row[0] if $start_time == 0;
		push @results, [$row[0] - $start_time, $row[1]];
	}
	close FILE;
}
for(@results) {
	print join(",",@{$_}) . "\n";
}
