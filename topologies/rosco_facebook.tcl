# Rosco - multi controller topology

set ns [new Simulator]
source tb_compat.tcl

set EDGE_SWITCH_COUNT 1
set TOR_SWITCH_COUNT 4
set CONTROLLERS 4

# Set up the switch nodes
set controlPlaneStr ""
for {set i 0} {$i < $EDGE_SWITCH_COUNT} {incr i} {
	set edsw($i) [$ns node]
	tb-set-node-os $edsw($i) Ubuntu1604-STD
	append controlPlaneStr "$edsw($i) "
}
for {set i 0} {$i < $TOR_SWITCH_COUNT} {incr i} {
	set host($i) [$ns node]
	tb-set-node-os $host($i) Ubuntu1604-STD
	set torsw($i) [$ns node]
	tb-set-node-os $torsw($i) Ubuntu1604-STD
	append controlPlaneStr "$torsw($i) "
	for {set j 0} {$j < $EDGE_SWITCH_COUNT} {incr j} {
		set link$i$j [$ns duplex-link $torsw($i) $edsw($j) 1000Mb 0ms DropTail]
	}
	set hostlink$i [$ns duplex-link $torsw($i) $host($i) 1000Mb 0ms DropTail]
}

# Set up the controllers
set consensusPlaneStr ""
for {set i 0} {$i < $CONTROLLERS} {incr i} {
	set snctl($i) [$ns node]
	tb-set-node-os $snctl($i) Ubuntu1604-STD
	append controlPlaneStr "$snctl($i) "
	append consensusPlaneStr "$snctl($i) "
}
set controlPlane [$ns make-lan "$controlPlaneStr" 1000Mbps 0ms]
set consensusPlane [$ns make-lan "$consensusPlaneStr" 1000Mbps 0ms]

#for {set i 0} {$i < $CONTROLLERS} {incr i} {
#	# Connect all controllers to each other
#	for {set j 0} {$j < $CONTROLLERS} {incr j} {
#		if {$i != $j} {
#			$snctl($i) add-route $snctl($j) $consensusPlane 
#		}
#	}
#	# Connect top of rack switches to controllers
#	for {set j 0} {$j < $TOR_SWITCH_COUNT} {incr j} {
#		$snctl($i) add-route $torsw($j) $controlPlane 
#		$torsw($j) add-route $snctl($i) $controlPlane 
#	}
#	# connect edge swithes to controllers
#	for {set j 0} {$j < $EDGE_SWITCH_COUNT} {incr j} {
#		$snctl($i) add-route $edsw($j) $controlPlane 
#		$edsw($j) add-route $snctl($i) $controlPlane 
#	}
#}

$ns run
