#!/usr/bin/perl

use strict;

use strict;
use Data::Dumper;
use File::Basename;
use Cwd 'abs_path';

my $target_image = "Ubuntu-16.04.qcow2";
my $target_location = "ams";
my $target_flavor = "c4r4h20";

my $script_dir = dirname(abs_path($0));
my $proj_dir = dirname($script_dir);

my $tor_count = shift or print_usage();
my $edge_count = shift or print_usage();
my $controller_count = shift or print_usage();

my $obj_name = sprintf "Rosco%dNode%dEdge%dController", $tor_count, $edge_count, $controller_count;
my $description = sprintf "Rosco Tree With %d Host Nodes %d Edge Nodes and %d Controllers", $tor_count, $edge_count, $controller_count;

print "$obj_name {\n";
print "\tdescription = \"$description\"\n";
print "\tid = \"$obj_name\"\n";

my @host_ports = map { "h$_" } (0 .. $tor_count - 1);
my @tor_ports =  map { "torsw$_" } (0 .. $tor_count - 1);
my @edge_ports = map { "ed$_" } (0 .. $edge_count - 1);
my @controller_ports = map { "c$_" } (0 .. $controller_count - 1);

for (0 .. $#host_ports) {
	my $host_name = $host_ports[$_];
	my $tor_name = $tor_ports[$_];
	print_host_declaration($host_name, ("$tor_name"));
	print_host_declaration($tor_name, ("$host_name", @edge_ports, @controller_ports));
	print_link($host_name, $tor_name);
}

for (@edge_ports) {
	my $edge_name = $_;
	print_host_declaration($edge_name, (@tor_ports, @controller_ports));
	for (@tor_ports) {
		print_link($edge_name, $_);
	}
}

for (0 .. $#controller_ports) {
	my $controller_name = $controller_ports[$_];
	print_host_declaration($controller_name, (@tor_ports, @edge_ports, @controller_ports));
	for (@tor_ports) {
		print_link($controller_name, $_);
	}
	for (@edge_ports) {
		print_link($controller_name, $_);
	}
	for ($_+1 .. $#controller_ports) {
		my $target_controller = $controller_ports[$_];
		print_link($controller_name, $target_controller);
	}
}

print "}\n";

sub print_link_connection {
	my $source = shift;
	my $target = shift;
	print "\tadjacency $source, $target\n";
}

sub print_host_declaration {
	my $host_name = shift;
	my @ports = @_;
	print "\tdef $host_name = host {\n";
	print "\t\tlocation = \"$target_location\"\n";
	print "\t\timageId = \"$target_image\"\n";
	print "\t\tflavorId = \"$target_flavor\"\n";
	print "\t\tid = \"$host_name\"\n";
	for(@ports) {
		print "\t\tport { id = \"$_\" }\n";
	}
	print "\t}\n";
}

sub print_link_declaration {
	my $link_name = shift;
	print "\tdef $link_name = link {\n";
	print "\t\tid = \"$link_name\"\n";
	print "\t\tport { id = \"src\" }\n";
	print "\t\tport { id = \"dst\" }\n";
	print "\t}\n";
}

sub print_link {
	my $source = shift;
	my $target = shift;
	my $link_name = sprintf "%sTO%s", $source, $target;
	print_link_declaration($link_name);
	print_link_connection("$source.$target", "$link_name.src");
	print_link_connection("$target.$source", "$link_name.dst");
}

sub print_usage {
	my $file_name = basename($0);
	die "$file_name tor_count edge_count controller_count\n";
}
