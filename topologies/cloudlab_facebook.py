"""Rosco Tree"""

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import the Emulab specific extensions.
import geni.rspec.emulab as emulab
import geni.rspec.pg as rspec

os_image = 'urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD'

control_plane_ip_count = 0
def get_cp_iface(name, node):
    global control_plane_ip_count
    cp_iface = node.addInterface(name, pg.IPv4Address('11.1.1.' + str(control_plane_ip_count),'255.255.255.0'))
    control_plane_ip_count += 1
    return cp_iface

def create_node(name, request, installscript):
    node = request.XenVM(name)
    node.cores = 4
    node.ram = 4096
    node.disk_image = os_image
    node.addService(rspec.Install(url="https://gitlab.com/robust-sdn/rosco/-/archive/master/rosco-master.tar.gz", path="/rosco"))
    return node

# Create a portal object,
pc = portal.Context()

# Describe the parameter(s) this profile script can accept.
pc.defineParameter( "h", "Number of Host Racks", portal.ParameterType.INTEGER, 4 )
pc.defineParameter( "f", "Number of Fabric Switches", portal.ParameterType.INTEGER, 4 )
pc.defineParameter( "c", "Number of Controllers", portal.ParameterType.INTEGER, 4 )
params = pc.bindParameters()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

# Check parameter validity.
if params.h < 1:
    pc.reportError( portal.ParameterError( "You must use at least 1 host rack" ) )
if params.f < 1:
    pc.reportError( portal.ParameterError( "You must use at least 1 fabric switch" ) )
if params.c < 1:
    pc.reportError( portal.ParameterError( "You must use at least 1 controller" ) )

link_control_plane = request.Link('control-plane')
link_agreement_plane = request.Link('agreement-plane')

# Create a nodes for controllers and add to network LAN
for i in range(params.c):
    controller = create_node("c" + str(i), request, "controllerinstall")
    ag_iface = controller.addInterface('agreement-plane-c' + str(i), pg.IPv4Address('11.1.2.' + str(i),'255.255.255.0'))
    link_agreement_plane.addInterface(ag_iface)
    cp_iface = get_cp_iface('control-plane-c' + str(i), controller)
    link_control_plane.addInterface(cp_iface)


# Create a nodes for tor switches, hosts, and add to network LAN
torsw_nodes = []
for i in range(params.h):
    # Create TOR
    torsw = create_node("torsw" + str(i), request, "switchinstall")
    torsw_nodes.append(torsw)
    cp_iface = get_cp_iface('control-plane-torsw' + str(i), torsw)
    link_control_plane.addInterface(cp_iface)

    # Create Host
    host = create_node("h" + str(i), request, "switchinstall")

    # Connect Host to TOR
    torsw_iface = torsw.addInterface('torsw-host-' + str(i))
    host_iface = host.addInterface('host-torsw' + str(i), pg.IPv4Address('10.' + str(i) + '.1.1','255.255.255.0'))
    host_torsw_link = request.Link('host-torsw-link-' + str(i), members=[torsw_iface, host_iface])
    host_torsw_link.disableMACLearning()

# Create a nodes for fabric switches and add to network LAN
for i in range(params.f):
    fbsw_name = 'fbsw' + str(i)
    fabric_node = create_node(fbsw_name, request, "hostinstall")
    cp_iface = get_cp_iface('control-plane-fbsw' + str(i), fabric_node)
    link_control_plane.addInterface(cp_iface)
    for j in range(params.h):
        torsw_name = 'torsw' + str(j)
        torsw_iface = torsw_nodes[j].addInterface(torsw_name + '-' + fbsw_name)
        fabric_iface = fabric_node.addInterface(fbsw_name + '-' + torsw_name)
        fabric_torsw_link = request.Link(fbsw_name + '-' + torsw_name + '-link', members=[torsw_iface, fabric_iface])
        fabric_torsw_link.disableMACLearning()

# Print the generated rspec
pc.printRequestRSpec(request)
