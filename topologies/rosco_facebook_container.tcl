# Rosco - multi controller topology

set ns [new Simulator]
source tb_compat.tcl

set EDGE_SWITCH_COUNT 1
set TOR_SWITCH_COUNT 7
set HOST_COUNT 1
set CONTROLLERS 4

# Set up the switch nodes
set controlPlaneStr ""
for {set i 0} {$i < $EDGE_SWITCH_COUNT} {incr i} {
	set edsw($i) [$ns node]
	tb-add-node-attribute $edsw($i) containers:node_type qemu
	#tb-add-node-attribute $edsw($i) "containers:partition" 100
	tb-set-node-os $edsw($i) Ubuntu1604-STD
	append controlPlaneStr "$edsw($i) "
}
for {set i 0} {$i < $TOR_SWITCH_COUNT} {incr i} {
	set torsw($i) [$ns node]
	tb-add-node-attribute $torsw($i) containers:node_type qemu
	#tb-add-node-attribute $torsw($i) "containers:partition" $i
	tb-set-node-os $torsw($i) Ubuntu1604-STD
	append controlPlaneStr "$torsw($i) "
	for {set j 0} {$j < $EDGE_SWITCH_COUNT} {incr j} {
		$ns duplex-link $torsw($i) $edsw($j) 1000Mb 0ms DropTail
	}
	for {set j 0} {$j < $HOST_COUNT} {incr j} {
		set idx [expr $i * $HOST_COUNT + $j]
		set host($idx) [$ns node]
		tb-add-node-attribute $host($idx) containers:node_type qemu
		#tb-add-node-attribute $host($idx) "containers:partition" $i
		tb-set-node-os $host($idx) Ubuntu1604-STD
		$ns duplex-link $host($idx) $torsw($i) 1000Mb 0ms DropTail
	}
}

# Set up the controllers
set consensusPlaneStr ""
for {set i 0} {$i < $CONTROLLERS} {incr i} {
	set snctl($i) [$ns node]
	tb-add-node-attribute $snctl($i) containers:node_type embedded_pnode
	#tb-add-node-attribute $snctl($i) containers:node_type qemu
	#tb-add-node-attribute $snctl($i) "containers:partition" [expr 101 + $i]
	#tb-set-node-os $snctl($i) Ubuntu1604-STD
	append controlPlaneStr "$snctl($i) "
	append consensusPlaneStr "$snctl($i) "
}
set controlPlane [$ns make-lan "$controlPlaneStr" 1000Mbps 0ms]
set consensusPlane [$ns make-lan "$consensusPlaneStr" 1000Mbps 0ms]

$ns rtproto Static
$ns run
