# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
#set term svg fname "Times,25" size 750, 400
#set term svg fname "Times,25" size 500, 400
set term svg fname "Times,21" size 550, 350

unset key
#set key at 29,74 spacing 0.7

set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on

set yrange [0:100]

set ytics offset graph 0.01,0
set xtics offset 0,graph 0.06

set tmargin at screen 0.97
set rmargin at screen 0.98
set lmargin at screen 0.13
set bmargin at screen 0.15

# Create the plot
set xlabel "Workload Duration (s)" offset 0,1.3 font ",30"
set ylabel "OVS CPU Utilization" offset graph 0.10,0 font ",30"

#plot dat_file using ($1/1000):2 title "Centralized" with lines lw 2 lc 1, \
#           '' using ($1/1000):5 title "Crash Tolerant" with lines lw 2 lc 4, \
#           '' using ($1/1000):3 title "SERENE" with lines lw 2 lc 2, \
#           '' using ($1/1000):4 title "SERENE Agg" with lines lw 2 lc 3, \
#           '' using ($1/1000):6 title "SERENE w/ Echo" with lines lw 2 lc 5, \
#           '' using ($1/1000):7 title "SERENE Agg w/ Echo" with lines lw 2 lc 6, \

ec = 20
poi = 2
plot dat_file every ec using ($1/1000):2 title "Centralized" with linespoints lw 2 lc 1 pt 7 ps 0.8 pi 500 pointinterval poi, \
           '' every ec using ($1/1000):5 title "Crash Tolerant" with linespoints lw 2 lc 4 pt 5 ps 0.8 pi 500 pointinterval poi, \
           '' every ec using ($1/1000):3 title "SERENE" with linespoints lw 2 lc 2 pt 9 ps 0.8 pi 500 pointinterval poi, \
           '' every ec using ($1/1000):4 title "SERENE Agg" with linespoints lw 2 lc 3 pt 11 ps 0.8 pi 500 pointinterval poi, \
           '' every ec using ($1/1000):6 title "SERENE w/ Echo" with linespoints lw 2 lc 7 pt 8 ps 1 pi 500 pointinterval poi, \
           '' every ec using ($1/1000):7 title "SERENE Agg w/ Echo" with linespoints lw 2 lc 6 pt 10 ps 1 pi 500 pointinterval poi, \
