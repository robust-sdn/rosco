# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,21" size 500, 400
set key on top left Left reverse spacing 0.65 samplen 2
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set format y "%.0f"
set yrange [0:*]
set xrange [0:*]

set ytics offset graph 0.02,0
set xtics offset 0,graph 0.05

set tmargin at screen 0.98
set rmargin at screen 0.98
set lmargin at screen 0.16
set bmargin at screen 0.14

# Create the plot
set xlabel "Timeout (ms)" offset 0,1.3
set ylabel "Batch Size" offset 2.5,0

plot dat_file using 1:2 title "CPython" with linespoints ps 0.5 pt 11 lw 3 lc 6, \
		   '' using 1:2:3:4 notitle with errorbars ps 0.5 pt 11 lw 1 lc 6, \
		   '' using 1:5 title "CPython First" with linespoints ps 0.5 pt 11 lw 3 lc 8
#			  using 1:5 title "RoSCo NO ACK" with linespoints ps 0.5 pt 7 lw 3 lc 5, \
#           '' using 1:5:6:7 notitle with errorbars ps 0.5 lw 1 lc 5, \
#		   '' using 1:8 title "RoSCo NO ACK Attack" with linespoints ps 0.5 pt 9 lw 3 lc 6, \
#		   '' using 1:8:9:10 notitle with errorbars ps 0.5 pt 9 lw 1 lc 6, \
