# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,21" size 200, 200
#set term svg fname "Times,21" size 600, 400
set key inside bottom right                 # No key
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set ytics 100,100 offset graph 0.10,0
set xtics 10,10 offset 0,graph 0.20

set yrange [1:*]
set format y "%1.0fK"
set xrange [1:48]

set tmargin at screen 0.86
set rmargin at screen 0.99
set lmargin at screen 0.35
set bmargin at screen 0.30

# Create the plot
set xlabel "# of Racks" offset screen 0,0.25
set ylabel "# of Routes" offset screen 0.21,0

plot dat_file using 1:($2/1000) notitle with lines lw 2
