# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,25" size 500, 400
#set term svg fname "Times,21" size 600, 400
unset key                   # No key
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set format y "%.0f"
set ytics 20,20 offset graph 0.03,0
set xtics 100, 200 offset 0,graph 0.06

set yrange [0:*]
set xrange [0:1200]

set tmargin at screen 0.98
set rmargin at screen 0.97
set lmargin at screen 0.13
set bmargin at screen 0.14

# Create the plot
set xlabel "Batch Size" offset 0,1.3
set ylabel "Latency (ms)" offset 3.0,0

plot dat_file using 1:2 title 'RoSCo' with linespoints pt 7 ps 0.5 lw 2 lc 8, dat_file with errorbars ps 0 lc 8 lw 1 
