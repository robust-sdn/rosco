# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,19" size 300, 50
set key outside center Right   # Place the key in an appropriate place
#set key samplen 2
#set key width -4.4

set yrange [1:2]

set notitle
set noborder
set noxtics
set noytics
set notitle
set noxlabel
set noylabel

plot 0 title "        SERENE Agg" with linespoints lw 2 lc 3 pt 11 ps 0.8 pi 500
