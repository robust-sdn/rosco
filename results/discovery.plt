# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
#set term svg fname "Times,25" size 500, 400
set term svg fname "Times,21" size 550, 350
unset key
set key inside bottom right spacing 0.7
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on

set yrange [0:1]
set xrange [1.2:2.3]

set ytics offset graph 0.01,0
set xtics offset 0,graph 0.06

set tmargin at screen 0.98
set rmargin at screen 0.98
set lmargin at screen 0.12
set bmargin at screen 0.16

# Create the plot
set xlabel "Topology Discovery Time (s)" offset 0,1.3 font ",30"
set ylabel "CDF" offset graph 0.10,0 font ",30"

plot dat_file using 1:3 title 'Centralized' with linespoints lw 2 lc 1 pt 7 ps 0.8 pi 10, \
           '' using 2:3 title 'Crash Tolerant' with linespoints lw 2 lc 4 pt 5 ps 0.8 pi 10, \
           '' using 4:6 title 'SERENE' with linespoints lw 2 lc 2 pt 9 ps 0.8 pi 10, \
           '' using 5:6 title 'SERENE Agg' with linespoints lw 2 lc 3 pt 11 ps 0.8 pi 10
