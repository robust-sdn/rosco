# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
#set term svg fname "Times,25" size 500, 400
set term svg fname "Times,21" size 550, 350
unset key
set key inside bottom right spacing 0.7 samplen 2.5
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on

set yrange [0:1]
set xrange [20:100]

set ytics 0,0.2 offset graph 0.01,0
set xtics (20, 30, 40, 50, 60, 70, 80, 90) offset 0,graph 0.06

set tmargin at screen 0.98
set rmargin at screen 0.98
set lmargin at screen 0.12
set bmargin at screen 0.16

# Create the plot
set xlabel "Flow Completion Time (ms)" offset 0,1.3 font ",30"
set ylabel "CDF" offset graph 0.10,0 font ",30"

plot dat_file using 1:8 title "Centralized" with linespoints lw 2 lc 1 pt 7 ps 0.8 pi 500, \
           '' using 6:8 title "SERENE MD" with linespoints lw 2 lc 2 pt 9 ps 0.8 pi 500, \
           '' using 7:8 title "SERENE Agg MD" with linespoints lw 2 lc 3 pt 11 ps 0.8 pi 500, \
           #'' using 3:8 title "SERENE MD" with linespoints lw 2 lc 2 pt 9 ps 0.8 pi 500, \
           #'' using 4:8 title "SERENE Agg MD" with linespoints lw 2 lc 3 pt 11 ps 0.8 pi 500, \
		   #'' using 2:8 title "Crash Tolerant" with linespoints lw 2 lc 4 pt 5 ps 0.5 pi 500, \
		   #'' using 1:8 tilte "Centralized" with linespoints lw 2 lc 1 pt 7 ps 0.8 pi 500, \
