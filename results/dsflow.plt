# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,25" size 500, 400
#unset key
set key inside top right spacing 0.7
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set ytics offset graph 0.03,0
set xtics offset 0,graph 0.06

set tmargin at screen 0.98
set rmargin at screen 0.97
set lmargin at screen 0.13
set bmargin at screen 0.16

# Create the plot
set xlabel "Workload Duration (s)" offset 0,1.3 font ",30"
set ylabel "Flow Completion Time (ms)" offset graph 0.18,0 font ",30"

plot dat_file using ($1/1000):2 title "Centralized" with lines lw 2 lc 1, \
           '' using ($7/1000):8 title "Crash Tolerant" with lines lw 2 lc 4, \
           '' using ($3/1000):4 title "SERENE" with lines lw 2 lc 2, \
           '' using ($5/1000):6 title "SERENE Agg" with lines lw 2 lc 3, \
