# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,25" size 500, 400
#set term svg fname "Times,21" size 600, 400
set key inside bottom right                 # No key
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on
set ytics 0,0.2 offset graph 0.03,0
set xtics offset 0,graph 0.06

set yrange [0:1]

set tmargin at screen 0.98
set rmargin at screen 0.97
set lmargin at screen 0.13
set bmargin at screen 0.16

# Create the plot
set xlabel "Topology Discovery Time(s)" offset 0,1.3
set ylabel "CDF" offset graph 0.17,0

plot dat_file using 1:3 title 'SERENE' with lines lw 2 lc 2, \
           '' using 2:3 title 'SERENE Agg' with lines lw 2 lc 3
