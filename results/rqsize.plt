# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
#set term svg fname "Times,25" size 500, 400
set term svg fname "Times,21" size 550, 350
unset key
set key at 9.5,95 spacing 0.7
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on

set yrange [0:110]
#set xrange [0.1:11]

set ytics offset graph 0.01,0
set xtics offset 0,graph 0.06

set tmargin at screen 0.98
set rmargin at screen 0.98
set lmargin at screen 0.12
set bmargin at screen 0.15

# Create the plot
set xlabel "Number of Domains" offset 0,1.3 font ",30"
set ylabel "% of Requests" offset 3.5,0 font ",30"

plot dat_file using 1:($2*100) title "Single Domain" with linespoints ps 0.8 pt 7 lw 3 lc 1, \
           '' using 1:($3*100) title "MD Hadoop" with linespoints ps 0.8 pt 9 lw 3 lc 2, \
		   '' using 1:($4*100) title "MD Webserver" with linespoints ps 0.8 pt 11 lw 3 lc 3, \
