# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
#set term svg fname "Times,25" size 500, 400
set term svg fname "Times,21" size 550, 350
unset key
set key inside bottom right spacing 0.7
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set grid                    # Turn the grid on

set yrange [0:8.75]
set xrange [0.1:11]

set ytics 0,1 offset graph 0.01,0
set xtics (1,4,5,6,7,8,9,10) offset 0,graph 0.06

set tmargin at screen 0.98
set rmargin at screen 0.98
set lmargin at screen 0.12
set bmargin at screen 0.14

# Create the plot
set xlabel "Control Plane Size" offset 0,1.3 font ",30"
set ylabel "Update Time (ms)" offset 2.0,0 font ",30"

plot dat_file using 5:6 title "Centalized" with points ps 0.8 pt 7 lc 1, \
           '' using 1:4 title "Crash Tolerant" with linespoints ps 0.8 lw 3 pt 5 lc 4, \
           '' using 1:2 title "SERENE" with linespoints ps 0.8 pt 9 lw 3 lc 2, \
           '' using 1:3 title "SERENE Agg" with linespoints ps 0.8 lw 3 pt 11 lc 3, \
