# Set formatting
set output out_file         # Set the output path

# Set the font to something pleasing
set term svg fname "Times,25" size 500, 400
unset key
#set key inside bottom right
set autoscale xfix          # Set axes automatically but fix X range to min and max value
unset logscale              # Clear log scale
set logscale y
set grid                    # Turn the grid on
set ytics offset graph 0.03,0
set xtics offset 0,graph 0.06

set yrange [0.01:10]

set tmargin at screen 0.98
set rmargin at screen 0.97
set lmargin at screen 0.13
set bmargin at screen 0.16

# Create the plot
set xlabel "Workload Duration (s)" offset 0,1.3
set ylabel "Flow Completion Time (s)" offset graph 0.22,0

plot dat_file using ($1/1000):($2/1000) with lines lw 2 lc (((line_color-1) % 3) + 1)
