#!/usr/bin/python

# ROSCO Upright Server Benchmark

import socket
    
# Test with larger PortStatus Message
#msgft=mt = "0|~|%d|~|{'xid': %d, 'msg_type': 12, 'msg_len': 80, 'datapath': 15, 'version': 4, 'buf': '\\x04\\x0c\\x00P\\x00\\x00\\x02(\\x02\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\xff\\xff\\xff\\xfe\\x00\\x00\\x00\\x00\\xde\\x94}q\\xe7D\\x00\\x00s15\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x01\\x00\\x00\\x00\\x01\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00\\x00', 'cls': 'OFPPortStatus'}\n"
    
# Test with PacketIn Message
msgfmt = "0|~|%d|~|{'xid': %d, 'msg_type': 10, 'msg_len': 34, 'datapath': 1, 'version': 4, 'buf': '\\x04\\n\\x00\"\\x00\\x00\\x00\\x06\\x00\\x00\\x01\\x02\\x00\\x00\\x00\\x00\\xff\\xff\\xff\\xff\\xff\\xff\\xff\\xff\\x00\\x01\\x00\\x04\\x00\\x00\\x00\\x00\\x00\\x00\\x04\\n\\x00\"\\x00\\x00\\x00\\x07', 'cls': 'OFPPacketIn'}\n"

msg_count = 10

def socket_send(sock, send_data):
    totalsent = 0
    while totalsent < len(send_data):
        sent = sock.send(send_data[totalsent:])
        if sent == 0:
            raise RuntimeError("socket connection broken")
        totalsent = totalsent + sent
        print totalsent;

def socket_recv_data(sock, size):
    recv_data = ""
    size_remaining = size
    while(size_remaining > 0):
        chunk = sock.recv(size_remaining)
        if chunk == '':
            raise RuntimeError("socket connection broken")
        recv_data += chunk
        size_remaining -= len(chunk)
    return recv_data

def socket_recv_line(sock):
    recv_data = ""
    while "\n" not in recv_data:
        chunk = sock.recv(1)
        if chunk == '':
            raise RuntimeError("socket connection broken")
        recv_data += chunk
    return recv_data.rstrip('\n')

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(("localhost", 5000))

for x in range(0, msg_count):
    msg = msgfmt % (x, x)
    socket_send(sock, "%d\n" % (len(msg)+1))
    socket_send(sock, "%s\n" % msg)

for x in range(0, msg_count):
    msglen = int(socket_recv_line(sock))
    msg = socket_recv_data(sock, msglen)
    msg = msg.rstrip('\n')
    print msg 

sock.shutdown(0)
sock.close()
