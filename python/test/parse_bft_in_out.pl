#!/usr/bin/perl

use strict;

my $in_time = 0;
while(<>) {
	if(/IN: (\d+)/) {
		$in_time = $1;
	}
	if(/OUT: (\d+) (\d+)/) {
		if($in_time eq $1) {
			print $2 - $in_time;
			print "\n";
		}
	}
}
