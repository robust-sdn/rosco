#!/usr/bin/perl

use strict;
use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/common.pl";

my $ctl_app = shift or die "Usage: startallctl.pl controller_app [log_level] [dpset_size] [batch_size] [batch_timeout]\n";
my $log_level = shift;
my $dpset_size = shift;
my $batch_size = shift;
my $batch_timeout = shift;
startControllers($ctl_app, $log_level, $dpset_size, $batch_size, $batch_timeout);
