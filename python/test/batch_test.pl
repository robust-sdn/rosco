#!/usr/bin/perl

use strict;
use Data::Dumper;

$| = 1;

my $base_dir = "/proj/Rosco/rosco";
my $rosco_app_dir = "$base_dir/sdn-scripts/RoscoApp";
my $scripts_dir = "$rosco_app_dir/test";
my $smart_bft_dir = "$base_dir/smartBFT";
my $host_config = "$smart_bft_dir/config/hosts.config";
#my $pythonpath = "/proj/Rosco/pypy/bin/pypy";
my $pythonpath = "/usr/bin/python3";

my $event_queue_command = "$pythonpath $rosco_app_dir/rosco_event_log.py";

my $iterations = 25;
my $batches = 1;
my @batch_range = (1, map{ $_ * 100 } (1 .. 30));
#my @batch_range = map{ $_ * 100 } (20 .. 30);
#my @batch_range = (1);
	
system("$scripts_dir/stopallbft.pl");

my $results;
for(@batch_range) {
	my $batch_size = $_;
	print "Running Batch Size: $batch_size...\n";
	$results->{$batch_size}->{'l'} = ();
	$results->{$batch_size}->{'b'} = ();
	$results->{$batch_size}->{'t'} = ();
	system("$scripts_dir/startallbft.pl");
	sleep 15;
	print "Controllers started...\n";
	for(1 .. $iterations) {
		print "$_...\n";
		my $total_latency = 0;
		my $total_batch = 0;
		for(split(/\n/, `$event_queue_command $batch_size 30 $batches`)) {
=pub
			if(/BATCH TIME: ([0-9.]+)/) {
				$total_batch = $total_batch + $1;
				#push @{$results->{$batch_size}->{'b'}}, $1;
			}
=cut
			if(/SENT LATENCY: ([0-9.]+)/) {
				$total_latency = $total_latency + $1;
				#push @{$results->{$batch_size}->{'l'}}, $1;
			}
			if(/THROUGHPUT: ([0-9.]+)/) {
				push @{$results->{$batch_size}->{'t'}}, $1;
			}
		}
		#push @{$results->{$batch_size}->{'b'}}, $total_batch / $iterations;
		push @{$results->{$batch_size}->{'l'}}, $total_latency / $iterations;
		sleep 1;
	}
	print_results($results);
	system("$scripts_dir/stopallbft.pl");
}

sub print_results {
	my $results = shift;
	for (sort { $a <=> $b } keys %{$results}) {
		print "l, $_, " . join(',', @{$results->{$_}->{'l'}}) . "\n";
		#print "b, $_, " . join(',', @{$results->{$_}->{'b'}}) . "\n";
		print "t, $_, " . join(',', @{$results->{$_}->{'t'}}) . "\n";
	}
}
