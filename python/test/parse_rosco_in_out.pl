#!/usr/bin/perl

use strict;

my $send_time = 0;
my $id = 0;
my $ack_times;
while(<>) {
	if(/TIME SEND: \d+ : (\d+) : \d+ : ([0-9.]+)/) {
		$send_time = $2;
		$id = $1;
	}
	if(/TIME ACK: \d+ : (\d+) : ([0-9].+)/) {
		if($1 eq $id) {
			print "$2 $send_time\n";
			my $time_diff = $2 - $send_time;
			if(!defined($ack_times->{$id}) || $ack_times->{$id} < $time_diff) {
				$ack_times->{$id} = $time_diff;
			}
		}
	}
}

for(sort {$a <=> $b} keys %{$ack_times}) {
	print "$ack_times->{$_}\n";
}
