from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
import pprint

import rosco_app

class SimpleSwitch13(rosco_app.RoscoApp):
    """Sample Rosco app: simple mac learner."""

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch13, self).__init__(*args, **kwargs)

        self.mac_to_port = {}
        self.msg_handlers = {"EventOFPPacketIn": self.packet_in_handler,
                "EventOFPSwitchFeatures": self.switch_features_handler,
                "EventOFPErrorMsg": self.error_msg_handler}

    def switch_features_handler(self, msg):
        print("Received EventOFPSwitchFeatures")

    def error_msg_handler(self, msg):
        from ryu import utils
        #print 'OFPErrorMsg received: type=0x%02x code=0x%02x message=%s' % (msg.type, msg.code, utils.hex_array(msg.data))

    def packet_in_handler(self, msg):

        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        dst = eth.dst
        src = eth.src

        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD
            #self.logger.info("packet in %s %s %s %s %s", dpid, src, dst, in_port, out_port)
            #self.logger.info(self.mac_to_port)

        actions = [parser.OFPActionOutput(out_port)]

        flow_mod = None

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            match = parser.OFPMatch(in_port=in_port, eth_dst=dst)
            flow_mod = self.add_flow(datapath, 1, match, actions)

        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                  in_port=in_port, actions=actions, data=data)
        if flow_mod is None:
            return (out, )
        else:
            return (flow_mod, out)


    def add_flow(self, datapath, priority, match, actions):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]

        mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                match=match, instructions=inst)

        return mod

