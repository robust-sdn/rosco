#!/usr/bin/python
import sys
import os
import time
from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, RemoteController
from mininet.link import TCLink
from mininet.log import setLogLevel
from mininet.cli import CLI
from facebook_topo import FacebookTopo, runHadoopFlows, runWebServerFlows
from subprocess import Popen, PIPE, STDOUT

PORT = 6633
iterations = 5

def _delete_if_exists(file_path):
    if os.path.exists(file_path):
        os.remove(file_path)

if __name__ == '__main__':
    setLogLevel('info')
    
    controllers = sys.argv[1:]

    for itr in range(0, iterations):
        flow_file = "flowout_%d" % itr
        _delete_if_exists(flow_file)
        net = Mininet(topo=FacebookTopo(), controller=RemoteController, link=TCLink, build=False)
        c = [ net.addController('c%s' % ci, controller=RemoteController,ip=controllers[ci-1], port=PORT)
                for ci in range(1, len(controllers) + 1) ]
    
        net.build()
        net.start()
        flow_rounds = runHadoopFlows(net, rounds=1, flows_per_round=300)
        #end_times, flow_times = runHadoopFlows(net, rounds=2, flows_per_round=20)
        #flow_times = runWebServerFlows(net)
        net.stop()

        if(flow_rounds is not None):
            flowout = open(flow_file, "w")
            for fround in flow_rounds:
                flowout.write(','.join(fround[0]))
                flowout.write('\n')
                flowout.write(','.join(fround[1]))
                flowout.write('\n')
            flowout.close()
   
#        if(flow_times is not None):
#            flowout = open(flow_file, "w")
#            flowout.write(','.join(end_times))
#            flowout.write('\n')
#            flowout.write(','.join(flow_times))
#            flowout.write('\n')
#            flowout.close()
