#!/usr/bin/python

from mininet.topo import Topo
from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, RemoteController
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.cli import CLI
import sys
import os
import time
import thread
from time import time, sleep
from select import poll, POLLIN
from subprocess import Popen, PIPE

"""
Author: Brian Lebiednik

Requires a custom controller with the command as follows on xterm1
./pox.py forwarding.l2_pairs openflow.discovery misc.full_payload openflow.of_01 --port=6653


Usage
sudo python performancetest.py

Currently you can change the following line to change the topology that the program runs

topo = {}(n=4)
{FatTreeTopotest, FatTreeTopo, Dcell, DcellNoLoop, Facebook, FatTreeTopoNoLoop}

To kill a mininet process running in the background issue a 'sudo mn -c'


"""

PORT = 6633

def iperf_thread(net, src, dst):
    host_pair = [src, dst]
    bandwidth = net.iperf(host_pair, seconds = 5)

def monitorFiles( outfiles, seconds, timeoutms ):
    "Monitor set of files and return [(host, line)...]"
    devnull = open( '/dev/null', 'w' )
    tails, fdToFile, fdToHost = {}, {}, {}
    for h, outfile in outfiles.iteritems():
        tail = Popen( [ 'tail', '-f', outfile ],
                      stdout=PIPE, stderr=devnull )
        fd = tail.stdout.fileno()
        tails[ h ] = tail
        fdToFile[ fd ] = tail.stdout
        fdToHost[ fd ] = h
    # Prepare to poll output files
    readable = poll()
    for t in tails.values():
        readable.register( t.stdout.fileno(), POLLIN )
    # Run until a set number of seconds have elapsed
    endTime = time() + seconds
    while time() < endTime:
        fdlist = readable.poll(timeoutms)
        if fdlist:
            for fd, _flags in fdlist:
                f = fdToFile[ fd ]
                host = fdToHost[ fd ]
                # Wait for a line of output
                line = f.readline().strip()
                yield host, line
        else:
            # If we timed out, return nothing
            yield None, ''
    for t in tails.values():
        t.terminate()
    devnull.close()  # Not really necessary

class Facebook(Topo):
    def build(self, n=2):
        """The baseline facebook topology has 4 fabric routers and 48 rack
        routers that make up a pod"""
        server_pods = n
        spine_routers = 4
        fabric_routers = 4
        rack_routers = 4
        link_bandwidth = 10
        link_delay = '.001ms'
        double_link_delay = '.002ms'
        #rack routers are also known as Top of the Rack (TOR) Routers

        spine_switches = {}
        fabric_switches = {}
        rack_switches = {}
        switch_type = OVSKernelSwitch;
        hosts = {}
        print '*** Adding the spine routers                              ***\n'
        for x in range(0, fabric_routers*spine_routers):
            spine_switches[x] = self.addSwitch('sr' + str(x), switch = switch_type)
        print '*** Adding the fabric routers                             ***\n'
        for x in range(0, server_pods*fabric_routers):
            fabric_switches[x] = self.addSwitch('fr' + str(x), switch = switch_type)
        print '*** Adding the rack routers                               ***\n'
        for x in range(0, server_pods*rack_routers):
            rack_switches[x] = self.addSwitch('rr' + str(x), switch = switch_type)
            hosts[x] = self.addHost('h'+str(x))
            self.addLink(hosts[x], rack_switches[x], bw=link_bandwidth, delay=link_delay)
        """
        *** Adding links to construct topology                ***
        *********************************************************
        *     cr1        cr2           cr3           cr4        *
        *                                                       *
        *                                                       *
        *                                                       *
        *                                                       *
        * er1  er2  er3  er4  er5  er6  er7  er8  er9 ...  er48 *
        *                                                       *
        *********************************************************
        """
        """ Connect spine routers to fabric routers           """
        for fr in range(0, fabric_routers):
            for pd in range(0, server_pods):
                for sr in range(0, spine_routers):
                    print "%d -> %d" % (fr*fabric_routers + sr, pd*fabric_routers + fr)
                    self.addLink(spine_switches[fr*fabric_routers + sr], fabric_switches[pd*fabric_routers + fr], bw=link_bandwidth*4, delay=double_link_delay)

        """ Connect rack routers to fabric routers            """
        for pd in range(0, server_pods):
            for fr in range(0, fabric_routers):
                #the fabric router that we want to connect
                for rr in range(0, rack_routers):
                    #the TOR router that we want to connect the fabric to
                    self.addLink(fabric_switches[pd*fabric_routers + fr], rack_switches[pd*rack_routers + rr], bw=link_bandwidth*4, delay=double_link_delay)

def perfTest():
    "Create network and run facebook performance test"
    
    pods = int(sys.argv[1])
    controllers = sys.argv[2:]

    topo = Facebook(n=pods)
    net = Mininet(topo=topo, controller=RemoteController, link=TCLink, build=False)
    c = [ net.addController('c%s' % ci, controller=RemoteController,ip=controllers[ci-1], port=PORT)
            for ci in range(1, len(controllers) + 1) ]

    net.build()
    net.start()
    seconds = 10
    #dumpNodeConnections(net.hosts) #Dumps the connections from each host
    net.waitConnected()

    CLI( net )
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    perfTest()
