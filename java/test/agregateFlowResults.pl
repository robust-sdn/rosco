#!/usr/bin/perl

use POSIX;
use List::Util qw(sum);

my $cdr_pieces = 10;

my @flow_durations = ();
while(<send_h*>) {
	for(split(/\n/, `ITGDec $_ 2>&1`)) {
		last if(/TOTAL RESULTS/);
		if (/Total time\s*=\s*([0-9\.]+)/) {
			push(@flow_durations, $1);
		}
	}
}
@flow_durations = sort {$a <=> $b} @flow_durations;
print join "\n", @flow_durations;

=pub
my $segments = (scalar @flow_durations) / $cdr_pieces;
for(0 .. $cdr_pieces-1) {
	my @slice = @flow_durations[$segments * $_ .. $segments * $_ + $segments];
	print median(@slice);
	print "\n";
}

sub median {
	sum( ( sort { $a <=> $b } @_ )[ int( $#_/2 ), ceil( $#_/2 ) ] )/2;
}
=cut
