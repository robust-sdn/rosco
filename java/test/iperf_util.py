import os
import time
import numpy as np
import re
import glob
from mininet.topo import Topo
from mininet.node import OVSKernelSwitch
from subprocess import Popen, PIPE, STDOUT

base_target_port = 10000
max_retries = 5
    
iperf_flows = {}
host_ports = []

def _start_iperf_recv(net):
    global host_ports
    print host_ports
    for h in range(0, len(net.hosts)):
        for port in range(base_target_port, host_ports[h]):
            command = "iperf -s -D -p %d" % port
            net.hosts[h].cmd(command)
def _stop_all_iperf():
    os.system("sudo killall -q iperf");
    os.system("sudo killall -q iperf_wrap");

def _searchForOutput(value):
    found_line = False
    lines = value.split("\n")
    for line in lines:
        if(len(line.strip()) > 0):
            return lines
    return None

def _dumpPopenResults(popen, results):
    stdout_lines = _searchForOutput(results[0])
    if(stdout_lines is not None):
        print "**** STDOUT: %s ****" % popen.host.name
        for line in stdout_lines:
            print line
        print "**** STDOUT: %s ****" % popen.host.name

    stderr_lines = _searchForOutput(results[1])
    if(stderr_lines is not None):
        print "**** STDERR: %s ****" % popen.host.name
        for line in stderr_lines:
            print line
        print "**** STDERR: %s ****" % popen.host.name

'''
def _start_iperf_send(net):
    global iperf_flows
    rsre = re.compile('^([0-9]+),([0-9]+)')
    popens = []
    end_times = []
    flow_times = []
    for h in net.hosts:
        for command in iperf_flows[h.name]:
            mypopen = h.popen(command)
            mypopen.host = h
            popens.append(mypopen)
    for mypopen in popens:
        results = mypopen.communicate()
        mypopen.wait()
        stdout = _searchForOutput(results[0])
        for line in stdout:
            rc = rsre.match(line)
            if (rc is not None):
                end_times.append(rc.group(1))
                flow_times.append(rc.group(2))
                break
        #_dumpPopenResults(mypopen, results);
    #print end_times
    #print flow_times
    return end_times,flow_times
'''

def _start_iperf_send(net):
    global iperf_flows
    #endm = re.compile('^([0-9]+),')
    #timem = re.compile('^([0-9]+)$')
    popens = []
    end_times = []
    flow_times = []
    for h in net.hosts:
        for command in iperf_flows[h.name]:
            mypopen = h.popen(command)
            mypopen.host = h
            popens.append(mypopen)
    for mypopen in popens:
        results = mypopen.communicate()
        mypopen.wait()
        stderr = _searchForOutput(results[1])
        if (stderr is not None):
            continue
        stdout = _searchForOutput(results[0])
        if(len(stdout) < 3):
            print "STDOUT ERROR: %s %d" % (mypopen.host.name, len(stdout))
            _dumpPopenResults(mypopen, results);
            continue
        flow_times.append(stdout[1])
        end_times.append(stdout[2])
        #for line in stdout:
        #    ec = endm.match(line)
        #    if (ec is not None):
        #        end_times.append(ec.group(1))
        #    tc = timem.match(line)
        #    if (tc is not None):
        #        flow_times.append(tc.group(1))
        #    break;
            #if(s.replace('.','',1).isdigit()):
            #    flow_times.append(s)
            #    break
        #_dumpPopenResults(mypopen, results);
    #print end_times
    #print flow_times
    return end_times,flow_times

def _runFlowsInstance(net):
    print "Starting iperf servers"
    _start_iperf_recv(net)
    time.sleep(10)
    print "Starting iperf senders"
    flow_times = _start_iperf_send(net)
    _stop_all_iperf()
    return flow_times

def runFlows(net, params, flows_per_host):
    expected_flows = flows_per_host * len(net.hosts)
    net.waitConnected()
    generate_flows(net, flows_per_round, params)
    for retry in range(0, max_retries):
        end_times, flow_times = _runFlowsInstance(net)
        time.sleep(1)
        if(len(flow_times) != expected_flows):
            print "Retrying, expected %d but got %d" % (expected_flows, len(flow_times))
        else:
            return end_times,flow_times
    print "Max retries hit"

def runFlowsRounds(net, params, rounds, flows_per_round):
    net.waitConnected()
    flow_rounds = []
    for r in range(0, rounds):
        print "Round %d" % r
        generate_flows(net, flows_per_round, params)
        end_times, flow_times = _runFlowsInstance(net)
        flow_rounds.append([end_times,flow_times])
    return flow_rounds

def _generate_iperf_flow(params, target_ip, target_port, delay):
    delay_seconds = float(delay) / 1000
    command = "./timens %f iperf -n %d -p %d -l %d -y c -c %s" % (delay_seconds, params['flow_size'], target_port, params['packet_size'], target_ip)
    #command = "./iperf_wrap %d %d %d %d %s" % (delay, params['flow_size'], target_port, params['packet_size'], target_ip)
    return command

def generate_flows(net, total_flows, params):
    global host_ports
    global iperf_flows
    print total_flows
    inter_arrival = params["inter_arrival"]
    in_rack_ratio = params["in_rack_ratio"]
    in_rack_params = params["in_rack_params"]
    out_of_rack_params = params["out_of_rack_params"]
    iperf_flows = {}
    host_ports = np.full(len(net.hosts), base_target_port, dtype=int)
    #host_port = base_target_port;
    for h in range (0, len(net.hosts)):
        iperf_cmds = []
        arrival_times = np.random.poisson(inter_arrival, total_flows)
        flow_distribution = np.random.rand(total_flows)
        total_delay = arrival_times[0]
        out_of_rack_target = out_of_rack_params["next_host"](h)
        in_rack_target = in_rack_params["next_host"](h)
        for i in range(0, total_flows):
            #target_port = host_port;
            #host_port += 1;
            if(flow_distribution[i] > in_rack_ratio):
                target_ip = net.hosts[out_of_rack_target].IP()
                target_port = host_ports[out_of_rack_target]
                iperf_cmd = _generate_iperf_flow(out_of_rack_params, target_ip, target_port, total_delay)
                #target_port = host_port;
                host_ports[out_of_rack_target] += 1
                out_of_rack_target = out_of_rack_params["next_host"](h, out_of_rack_target)
                iperf_cmds.append(iperf_cmd)
            else:
                target_ip = net.hosts[in_rack_target].IP()
                target_port = host_ports[in_rack_target]
                iperf_cmd = _generate_iperf_flow(in_rack_params, target_ip, target_port, total_delay)
                #target_port = host_port;
                host_ports[in_rack_target] += 1
                in_rack_target = in_rack_params["next_host"](h, in_rack_target)
                iperf_cmds.append(iperf_cmd)
            total_delay += arrival_times[i]
        iperf_flows[net.hosts[h].name] = iperf_cmds
    return total_flows
