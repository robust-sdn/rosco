#!/usr/bin/perl

use strict;
use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/common.pl";

print_usage() unless $#ARGV == 0;

my $type = shift;
stopControllers($type);

sub print_usage {
    my $file_name = basename($0);
    die "Stop all RoSCo Controllers\nUsage: $file_name type\n";
}
