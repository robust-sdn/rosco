#!/usr/bin/perl

use strict;
use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/common.pl";

print_usage() unless $#ARGV == 1;
my $type = shift;
my $ctl_app = shift;
startControllers($type, $ctl_app);

sub print_usage {
    my $file_name = basename($0);
    die "Start all RoSCo Controllers\nUsage: $file_name type controller_app\n";
}
