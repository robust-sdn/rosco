#!/usr/bin/perl

use strict;
use File::Basename;
use Cwd 'abs_path';
my $script_dir = dirname(abs_path($0));
require "$script_dir/common.pl";

print_usage() unless $#ARGV >= 3;

my $bls = 1;
my $batch = shift;
my $timeout = shift;
my $ack_type = shift;
my $ctl_app = shift;
my $log_level = shift;

startControllers($bls, $batch, $timeout, $ack_type, $ctl_app, $log_level);

sub print_usage {
    my $file_name = basename($0);
    die "Start all RoSCo Controllers\nUsage: $file_name max_batch batch_timeout ack_type controller_app\n";
}
