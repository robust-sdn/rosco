import os
import time
import numpy as np
import re
import glob
from mininet.topo import Topo
from mininet.node import OVSKernelSwitch
from subprocess import Popen, PIPE, STDOUT

base_target_port = 10000
max_flows_per_file = 300
max_retries = 5

def _start_itg_recv(net):
    for host in net.hosts:
        #command = "ITGRecv 2>&1 > itg_rcv_log_%s &" % host.name
        command = "ITGRecv 2>&1 > /dev/null &"
        host.cmd(command)
def _stop_itg_recv():
    os.system("sudo killall -q ITGRecv");

def _delete_if_exists(file_path):
    if os.path.exists(file_path):
        os.remove(file_path)

def _dumpPopenResults(popen, results):
    #for line in results[0].split("\n"):
    #    print line
    print "**** STDERR: %s ****" % popen.host.name
    for line in results[1].split("\n"):
        print line
    print "**** STDERR: %s ****" % popen.host.name

def _start_itg_send(net, r):
    popens = []
    for h in net.hosts:
        file_index = 0
        file_extension = "%s_%d_%d" % (h.name, r, file_index)
        while(os.path.exists("itg_%s" % file_extension)):
            command = "ITGSend itg_%s -l send_%s -x recv_%s" % (file_extension, file_extension, file_extension)
            mypopen = h.popen(command)
            mypopen.host = h
            popens.append(mypopen)
            file_index += 1
            file_extension = "%s_%d_%d" % (h.name, r, file_index)

    for mypopen in popens:
        results = mypopen.communicate()
        mypopen.wait()
        _dumpPopenResults(mypopen, results);
def _stop_itg_send():
    os.system("sudo killall -q ITGSend")

def _itg_cleanup(net):
    os.system("sudo rm itg_h*")
    #for h in net.hosts:
    #    _delete_if_exists("itg_%s" % (h.name))

def _get_itg_flow_times(r=None):
    endTimes = []
    flowTimes = []
    p = Popen(["./parse_itg_results.pl", "%d" % r], stdout=PIPE, stderr=STDOUT)
    results = p.communicate()
    p.wait()
    if (results[0] is not None):
        for line in results[0].split('\n'):
            lineReadings = line.split(',')
            if(len(lineReadings) > 1):
                endTimes.append(lineReadings[0])
                flowTimes.append(lineReadings[1])
    return endTimes,flowTimes

def _runFlowsInstance(net, r = 0):
    print "Starting ITG Recv"
    _start_itg_recv(net)
    time.sleep(10)
    print "Starting ITG Send"
    _start_itg_send(net, r)
    _stop_itg_recv()

def runFlows(net, flows_per_host):
    expected_flows = flows_per_host * len(net.hosts)
    net.waitConnected()
    for retry in range(0, max_retries):
        os.system("sudo rm send_h*")
        os.system("sudo rm recv_h*")
        _runFlowsInstance(net)
        flow_times = _get_itg_flow_times()
        if(len(flow_times) != expected_flows):
            print "Retrying, expected %d but got %d" % (expected_flows, len(flow_times))
        else:
            return flow_times
    print "Max retries hit"

def runFlowsRounds(net, params, rounds, flows_per_round):
    net.waitConnected()
    flow_rounds = []
    _clear_itg_flow_files()
    os.system("sudo rm send_h*")
    os.system("sudo rm recv_h*")
    for r in range(0, rounds):
        _generate_flows(net, flows_per_round, params, r)
    for r in range(0, rounds):
        print "Round %d" % r
        _runFlowsInstance(net, r)
        end_times, flow_times = _get_itg_flow_times(r)
        flow_rounds.append([end_times, flow_times])
    return flow_rounds

def _generate_itg_flow(fo, target_ip, target_port, delay, params):
    packet_count = params["flow_size"] // params["packet_size"]
    fo.write("-a %s -rp %d -z %d -T TCP -c %d -d %d\n" % (target_ip, target_port, packet_count, params["packet_size"], delay))
    #fo.write("-a %s -rp %d -z %d -T TCP -o %d -d %d\n" % (target_ip, target_port, packet_count, params["packet_size"], delay))
    #fo.write("-a %s -rp %d -t %d -z %d -T TCP -o %d -d %d\n" % (target_ip, target_port, params["flow_duration"], packet_count, params["packet_size"], delay))
    #fo.write("-a %s -z %d -T UDP -o %d -d %d\n" % (net.hosts[target_id].IP(), packet_count, params["packet_size"], delay))

def _clear_itg_flow_files():
    os.system("sudo rm itg_h*")

def _get_flow_file(hostname, r, index):
    filename = "itg_%s_%d_%d" % (hostname, r, index)
    _delete_if_exists(filename)
    return open(filename, "w")

def _generate_flows(net, total_flows, params, r=0):
    print total_flows
    inter_arrival = params["inter_arrival"]
    in_rack_ratio = params["in_rack_ratio"]
    in_rack_params = params["in_rack_params"]
    out_of_rack_params = params["out_of_rack_params"]
    #host_ports = np.full(len(net.hosts), base_target_port, dtype=int)
    host_port = base_target_port;
    for h in range (0, len(net.hosts)):
        arrival_times = np.random.poisson(inter_arrival, total_flows)
        flow_distribution = np.random.rand(total_flows)
        total_delay = 0
        file_index = 0
        file_flow_index = 0
        out_of_rack_target = None
        in_rack_target = None
        scr_file = None
        for i in range(0, total_flows):
            target_port = host_port;
            host_port += 1;
            if (scr_file is None):
                scr_file = _get_flow_file(net.hosts[h].name, r, file_index)
            if(flow_distribution[i] > in_rack_ratio):
                out_of_rack_target = out_of_rack_params["next_host"](h, out_of_rack_target)
                target_ip = net.hosts[out_of_rack_target].IP()
                #target_port = host_ports[out_of_rack_target]
                target_port = host_port;
                _generate_itg_flow(scr_file, target_ip, target_port, total_delay, out_of_rack_params)
                #host_ports[out_of_rack_target] += 1
            else:
                in_rack_target = in_rack_params["next_host"](h, in_rack_target)
                target_ip = net.hosts[in_rack_target].IP()
                #target_port = host_ports[in_rack_target]
                target_port = host_port;
                _generate_itg_flow(scr_file, target_ip, target_port, total_delay, in_rack_params)
                #host_ports[in_rack_target] += 1
            total_delay += arrival_times[i]
            file_flow_index += 1
            if (file_flow_index >= max_flows_per_file):
                file_flow_index = 0
                file_index += 1
                scr_file.close()
                scr_file = None
        if(scr_file is not None):
            scr_file.close()
    return total_flows
