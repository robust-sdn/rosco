#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <wait.h>
#include <sched.h>

#define IPERF "/usr/bin/iperf"
#define MSPERUSEC 1000
#define NSPERSEC 1000000000LL
#define MAX_TIME (12 * NSPERSEC)

static inline uint64_t getTOD()
{
	struct timespec cur_time;
	clock_gettime(CLOCK_REALTIME, &cur_time);
	return cur_time.tv_sec * 1000000000LL + cur_time.tv_nsec;
}

static inline char run_iperf(char* args[])
{
	/*
	for(int i = 2; i < 6; i++) {
		printf("%s\n", args[i]);
	}
	*/
	int ai = 2;
	char* iperf_args[] = {
		IPERF,
		"-n",
		args[ai++],
		"-p",
		args[ai++],
		"-l",
		args[ai++],
		"-y",
		"c",
		"-c",
		args[ai++],
		NULL,
	};
	int stdout_pipe[2];
	int stderr_pipe[2];
	char success = 1;
	if(pipe(stdout_pipe) == -1) {
		fprintf(stderr, "STDOUT Pipe Failed %s\n", strerror(errno));
		exit(1);
	}
	if(pipe(stderr_pipe) == -1) {
		fprintf(stderr, "STDERR Pipe Failed %s\n", strerror(errno));
		exit(1);
	}
	pid_t child_pid = fork();
	if(child_pid == -1) {
		fprintf(stderr, "Fork failed: %s\n", strerror(errno));
		exit(1);
	} else if(child_pid == 0) {
		close(stdout_pipe[0]);
		close(stderr_pipe[0]);
		dup2(stdout_pipe[1], STDOUT_FILENO);
		dup2(stderr_pipe[1], STDERR_FILENO);
		if(execv(IPERF, iperf_args) == -1){
			fprintf(stderr, "Exec failed: %s\n", strerror(errno));
			exit(1);
		}
	} else {
		close(stdout_pipe[1]);
		close(stderr_pipe[1]);
		close(stdout_pipe[0]);
		waitpid(child_pid, NULL, WNOHANG);
		char buf;
		success = read(stderr_pipe[0], &buf, 1) == 0;
		close(stderr_pipe[0]);
	}
	return success;
}

int main(int argc, char* argv[])
{
	int sleeptime = atoi(argv[1]) * MSPERUSEC;
	if(sleeptime > 0) {
		usleep(sleeptime);
	}
	uint64_t starttime = getTOD();
	if(run_iperf(argv) == 1) {
		uint64_t endtime = getTOD();
		printf("%" PRId64 ",%" PRId64 "\n", endtime, endtime - starttime);
	}
	return 0;
	/*
	int try_count = 0;
	uint64_t starttime = getTOD();
	while(run_iperf(argv) == 0) {
		sched_yield();
		if(getTOD() - starttime > MAX_TIME) {
			exit(1);
		}
	}
	uint64_t endtime = getTOD();
	printf("%" PRId64 ",%" PRId64 "\n", endtime, endtime - starttime);
	return 0;
	*/
}
