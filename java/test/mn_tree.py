#!/usr/bin/python

import sys
import os

from mininet.net import Mininet
from mininet.node import Controller, OVSKernelSwitch, RemoteController
from mininet.cli import CLI
from mininet.log import setLogLevel, info, output
from mininet.topolib import TreeNet
from time import sleep
import re
from scipy.stats.mstats import gmean
import numpy

PORT = 6633

def asyncPingAll( net, timeout=None ):
#output( 'Executing asyncPingAll\n' )
    hosts = net.hosts
    popens = []
    for node in hosts:
        for dest in hosts:
            if node != dest:
                #output( '%s -> %s\n' % (node.name, dest.name) )
                command = 'ping -c1 -W %d %s' % (timeout, dest.IP())
                mypopen = node.popen(command)
                mypopen.source = node.name
                mypopen.command = command
                popens.append(mypopen)
	#output( 'Waiting for results\n' )
    command_failures = 0
    rtt_readings = []
    for mypopen in popens:
        results = mypopen.communicate()
        retries = 0
        rtt = 0
        for line in results[0].split("\n"):
            match_rtt = re.match("rtt min/avg/max/mdev = ([0-9.]+)", line)
            if(match_rtt):
                rtt = float(match_rtt.group(1))
        retcode = mypopen.wait()
        if retcode > 0:
            output('Command failure on ping from %s: %s\n%s\n%s\n' % (mypopen.source, mypopen.command, results[0], results[1]))
            command_failures = command_failures + 1
        else:
            rtt_readings.append(rtt)

    #output( 'asyncPingAll Complete\n' )
    #output('There were %d failed commands\n' % command_failures)
    #output( 'Average RTT: %f\n' % (sum(rtt_readings)/float(len(rtt_readings))) )
    return [command_failures, gmean(numpy.array(rtt_readings))]

if __name__ == '__main__':
    
    if(len(sys.argv) < 4):
         raise SystemExit('Usage: %s depth fanout timeout controller1 controller2 ...' % os.path.basename(__file__))

    depth  = int(sys.argv[1])
    fanout = int(sys.argv[2])
    timeout = int(sys.argv[3])
    controllers = sys.argv[4:]

    net = TreeNet(depth=depth, fanout=fanout, controller=RemoteController, switch=OVSKernelSwitch, build=False)

    c = [ net.addController('c%s' % ci, controller=RemoteController,ip=controllers[ci-1], port=PORT)
            for ci in range(1, len(controllers) + 1) ]
   
    setLogLevel('debug')
    net.build()
    net.start()
    sleep(20)

    [command_failures, rtt] = asyncPingAll(net, timeout)
    #print "%f,%d,%f" % (rtt, command_failures, retries)
    print "%f,%d" % (rtt, command_failures)
    #print "%s, %s, %s" % (sys.argv[1], sys.argv[2], (",".join(map(str, rtt_readings))))

    net.stop()

