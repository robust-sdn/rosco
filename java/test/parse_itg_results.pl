#!/usr/bin/perl

use strict;
use Data::Dumper;

my $output_round = shift;

my $flow_start_times;
while(<itg_h*>) {
	my $itg_flow_file = $_;
	if(/(h\d+)_(\d+)_(\d+)/) {
		my $host = $1;
		my $round = $2;
		my $file_index = $3;
		my $flow_number = 1;
		for(split(/\n/, `cat $itg_flow_file`)) {
			chomp;
			if(/-d (\d+)/) {
				my $start_delay = $1;
				$flow_start_times->{$round}->{$start_delay}->{$host}->{$flow_number} = $file_index;
				$flow_number += 1;
			}
		}
	}
}
my $flow_durations;
while(<send_h*>) {
	my $send_flow_file = $_;
	if(/(h\d+)_(\d+)_(\d+)/) {
		my $host = $1;
		my $round = $2;
		my $file_index = $3;
		my $flow_number = -1;
		for(split(/\n/, `ITGDec $send_flow_file 2>&1`)) {
			chomp;
			last if(/TOTAL RESULTS/);
			if(/Flow number: (\d+)/) {
				$flow_number = $1;
			}
			if(/Total time\s*=\s*([0-9.]+)/ && $flow_number > 0) {
				$flow_durations->{$host}->{$round}->{$file_index}->{$flow_number} = $1;
				$flow_number = -1;
			}
		}
	}
}

my $round = $output_round;
#for(sort { $a<=> $b } keys %{$flow_start_times}) {
#	my $round = $_;
#	open(FLOWOUT, ">", "flowout_$round") or die "Unable to open flowout_$round: $!\n";
	for(sort { $a<=>$b} keys %{$flow_start_times->{$round}}) {
		my $start_delay = $_;
		for(keys %{$flow_start_times->{$round}->{$start_delay}}) {
			my $host = $_;
			for(keys %{$flow_start_times->{$round}->{$start_delay}->{$host}}) {
				my $flow_number = $_;
				my $file_index = $flow_start_times->{$round}->{$start_delay}->{$host}->{$flow_number};
				my $flow_duration = $flow_durations->{$host}->{$round}->{$file_index}->{$flow_number};
				if(defined($flow_duration)) {
					print "$start_delay,$flow_duration\n";
#					print FLOWOUT "$start_delay, $flow_duration\n";
				}
			}
		}
	}
#	close FLOWOUT;
#}
