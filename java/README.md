# rosco-java
Java layer of the RoSCo Robust SDN Controller

## Setup

Set up RSA key pairs for controllers so that ssh works without a password

Install java components
 * sudo apt-get install ant
 * sudo apt-get install default-jdk

Install Ryu 4.23 for python

Edit config/hosts.config to set the IP addresses of all replicas

## Test Scripts (in /test)
NOTE: ensure that /tmp/rosco/ does not exist on each controller

* startallctl.pl - start all RoSCo Controllers

  Usage: startallctl.pl batch_size batch_timeout ack_type controller_script
  
       Ack Types -
           0: NO ACK
           1: PAR ACK
           2: LNZ ACK
	   
  Example Usage: startallctl.pl 1000 100 0 cbench
* stopallctl.pl - stop all RoSCo Controllers

* startallqt.pl - start RoSCo Queue Test

  Results are printed in /tmp/rosco/logs/roscobft_0.log on replicate 0

  Usage: startallqt.pl batch_size batch_count
  
  Example Usage: startallqt.pl 1000 10
  
* stopallqt.pl - Stop RoSCo Queue Test

  Usage: startallqt.pl batch_size batch_count
  
  Example Usage: startallqt.pl 1000 10
