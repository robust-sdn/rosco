/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.io.FileOutputStream;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

import java.net.Socket;
import java.nio.BufferUnderflowException;

/**
 * Wrap an OpenFlow Message with a BLS signature based on the signature share
 *
 */

public class RoscoBLSConnection extends ProxyConnection
{
	protected int xNodeId;
	protected RoscoBLSSigner xSigner;

	protected static final Set<Byte> ROSCO_REPLY_TYPES = 
		new HashSet<Byte>(
			// TODO: ADD ALL APPROPRIATE MESSAGE TYPES
			Arrays.asList(
				OpenFlowMessage.OF_ROLEREQUEST,
				OpenFlowMessage.OF_PACKETOUT,
				OpenFlowMessage.OF_FLOWMOD
			)
		);

	public RoscoBLSConnection(int dp_idx, Socket switch_socket, Socket controller_socket, OpenFlowProxyHandler ofHandler, int node_id, RoscoBLSSigner signer)
	{
		super(dp_idx, switch_socket, controller_socket, ofHandler);
		xNodeId = node_id;
		xSigner = signer;
	}

	public void sendSwitch(OpenFlowMessage ofMsg)
	{
		if(ROSCO_REPLY_TYPES.contains(ofMsg.getType())) {
			super.sendSwitch(new OpenFlowBLSMessage(ofMsg, xNodeId, xSigner.signToBytes(ofMsg.getMessageBytes())));
		} else {
			super.sendSwitch(ofMsg);
		}	
	}
}

class RoscoBLSLeaderConnection extends RoscoBLSConnection
{
	private RoscoLeaderConnection xToLeader;

	public RoscoBLSLeaderConnection(int dp_idx, Socket switch_socket, Socket controller_socket, RoscoLeaderConnection leader, OpenFlowProxyHandler ofHandler, int node_id, RoscoBLSSigner signer)
	{
		super(dp_idx, switch_socket, controller_socket, ofHandler, node_id, signer);
		xToLeader = leader;
	}

	public void sendSwitch(OpenFlowMessage ofMsg)
	{
		if(ROSCO_REPLY_TYPES.contains(ofMsg.getType())) {
			xToLeader.sendLeader(new OpenFlowBLSLeaderMessage(ofMsg, xDpIndex, xNodeId, xSigner.signToBytes(ofMsg.getMessageBytes())));
		} else {
			super.sendSwitch(ofMsg);
		}	
	}
}

class OpenFlowBLSMessage extends OpenFlowMessage
{
	protected static final byte BLS_MSG_TYPE = (byte)0xAA;

	public OpenFlowBLSMessage(OpenFlowMessage source, int node_id, byte[] signature)
	{
		super(source.getVersion(), BLS_MSG_TYPE, (short)(OpenFlowMessage.OF_HEADER_SIZE + 2 + signature.length + source.getSize()), source.getXID());
		xMessageBody.put((byte)node_id);
		xMessageBody.put((byte)signature.length);
		xMessageBody.put(signature);
		xMessageBody.put(source.getMessageBytes());
		// RoscoLogger.logInfo("SIGSIZE: " + signature.length + " MSG SIZE: " + source.getSize());
	}
}

class OpenFlowBLSAggregateMessage extends OpenFlowMessage
{
	private static final byte BLS_AGG_MSG_TYPE = (byte)0xAB;
	private static final byte BLS_ROLE_MSG_TYPE = (byte)0xAD;

	public OpenFlowBLSAggregateMessage(OpenFlowMessage source, byte[] signature)
	{
		super(
			source.getVersion(),
			source.getType() == OpenFlowMessage.OF_ROLEREQUEST ? BLS_ROLE_MSG_TYPE : BLS_AGG_MSG_TYPE,
			(short)(OpenFlowMessage.OF_HEADER_SIZE + 1 + signature.length + source.getSize()),
			source.getXID()
		);
		xMessageBody.put((byte)signature.length);
		xMessageBody.put(signature);
		xMessageBody.put(source.getMessageBytes());
	}
}

class OpenFlowBLSLeaderMessage extends OpenFlowMessage
{
	private byte xDpIndex;
	protected byte xNodeId;
	protected int xSignatureSize;
	protected byte[] xSignature;
	protected OpenFlowMessage xSource;
	
	protected static final byte BLS_LDR_MSG_TYPE = (byte)0xAC;

	public OpenFlowBLSLeaderMessage(OpenFlowMessage message)
	{
		super(message.xVersion, message.xType, (short)0, message.xXid);
		try {
			xDpIndex = message.xMessageBody.get();
			xNodeId = message.xMessageBody.get();
			xSignatureSize = message.xMessageBody.getInt();
			xSignature = new byte[xSignatureSize];
			message.xMessageBody.get(xSignature);
			xSource = new OpenFlowMessage(message.xMessageBody);
		} catch(BufferUnderflowException e) {
			e.printStackTrace();
		}
	}

	public OpenFlowBLSLeaderMessage(OpenFlowMessage source, int dp_idx, int node_id, byte[] signature)
	{
		super(source.getVersion(), BLS_LDR_MSG_TYPE, (short)(OpenFlowMessage.OF_HEADER_SIZE + 6 + signature.length + source.getSize()), source.getXID());
		xMessageBody.put((byte)dp_idx);
		xMessageBody.put((byte)node_id);
		xMessageBody.putInt(signature.length);
		xMessageBody.put(signature);
		xMessageBody.put(source.getMessageBytes());
		///System.out.println("CONSTRUCT LEADER MESSAGE: " + this + source);
	}
	
	public byte getDpIndex()
	{
		return xDpIndex;
	}

	public byte getNodeId()
	{
		return xNodeId;
	}

	public byte[] getSignature()
	{
		return xSignature;
	}

	public OpenFlowMessage getMessage()
	{
		return xSource;
	}
}
