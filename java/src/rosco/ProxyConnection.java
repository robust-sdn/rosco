/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
:limitations under the License.
*/
package rosco;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.nio.ByteBuffer;

/**
 * Openflow Proxy Connection
 *
 */

class ProxyConnection
{
	private boolean xRunning;
	protected int xDpIndex;
	protected long xDpId;

	private Socket xSwitchSocket;
	private SwitchConnectionThread xSwitchThread;
	private OpenFlowMessageReader xFromSwitchStream;
	private OpenFlowMessageWriter xToSwitchStream;
	private byte xSwitchVersion;

	private Socket xControllerSocket;
	private ControllerConnectionThread xControllerThread;
	private OpenFlowMessageReader xFromControllerStream;
	private OpenFlowMessageWriter xToControllerStream;
	private byte xControllerVersion;

	private OpenFlowProxyHandler xOpenFlowHandler;

	ProxyConnection(int dp_idx, Socket switch_socket, Socket controller_socket, OpenFlowProxyHandler ofHandler)
	{
		xDpId = -1;
		xDpIndex = dp_idx;
		xOpenFlowHandler = ofHandler;
		xSwitchVersion = 0;
		xControllerVersion = 0;
		xRunning = false;

		if(switch_socket != null) {
			xSwitchSocket = switch_socket;
			try {
				xFromSwitchStream = new OpenFlowMessageReader(switch_socket.getInputStream());
				xToSwitchStream = new OpenFlowMessageWriter(switch_socket.getOutputStream());
				xSwitchThread = new SwitchConnectionThread(this);
			} catch(IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
		
		if(controller_socket != null) {
			xControllerSocket = controller_socket;
			try {
				xFromControllerStream = new OpenFlowMessageReader(controller_socket.getInputStream());
				xToControllerStream = new OpenFlowMessageWriter(controller_socket.getOutputStream());
				xControllerThread = new ControllerConnectionThread(this);
			} catch(IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	public long getDpId()
	{
		// System.out.println(xDpId);
		return xDpId;
	}

	public String toString()
	{
		return "ProxyConnection: SW: " + xSwitchSocket + " CTL: " + xControllerSocket;
	}

	protected void socketDisconnect(Socket sock)
	{
		try {
			System.out.println("DISCONNECTING FROM: " + sock);
			sock.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public void disconnect()
	{
		socketDisconnect(xSwitchSocket);
		socketDisconnect(xControllerSocket);
		xRunning = false;
	}

	public void sendSwitch(OpenFlowMessage ofMsg)
	{
		/*
		if(ofMsg.getType() != OpenFlowMessage.OF_HELLO) {
			System.out.println(xDpId + " MSG TO SW: " + ofMsg);
		}
		*/
		if(xToSwitchStream.write(ofMsg) == false) {
			disconnect();
		}
	}

	public void sendController(OpenFlowMessage ofMsg)
	{
		/*
		if(ofMsg.getType() != OpenFlowMessage.OF_HELLO) {
			System.out.println("MSG TO CTL: " + ofMsg);
		}
		*/
		if(xToControllerStream.write(ofMsg) == false) {
			disconnect();
		}
	}

	public byte getNegotiatedVersion()
	{
		if(xSwitchVersion < xControllerVersion) {
			return xSwitchVersion;
		}
		return xControllerVersion;
	}

	public void start()
	{
		xRunning = true;
		if(xSwitchThread != null) {
			new Thread(xSwitchThread, "PROXY SWITCH: " + xSwitchSocket).start();
		}
		if(xControllerThread != null) {
			new Thread(xControllerThread, "PROXY CONTROLLER " + xControllerSocket).start();
		}
		xOpenFlowHandler.handleNewConnection(this);
	}

	class SwitchConnectionThread implements Runnable
	{
		private ProxyConnection xProxy;

		SwitchConnectionThread(ProxyConnection conn)
		{
			xProxy = conn;
		}

		private void setDpIdFromMsg(OpenFlowMessage ofMsg)
		{
			byte[] msg = ofMsg.getMessageBytes();
			byte[] dpid = Arrays.copyOfRange(msg, OpenFlowMessage.OF_HEADER_SIZE, OpenFlowMessage.OF_HEADER_SIZE + 8);
			ByteBuffer dpidbb = ByteBuffer.wrap(dpid);
			xDpId = dpidbb.getLong();
			// System.out.println("FOUND NEW DPID: " + xDpId);
		}
		
		public void run()
		{
			while(xRunning) {
				OpenFlowMessage ofMsg = xFromSwitchStream.read();
				if(ofMsg == null) {
					xProxy.disconnect();
					return;
				}
				if(ofMsg.getType() == OpenFlowMessage.OF_HELLO) {
					xSwitchVersion = ofMsg.getVersion();
				}
				if(ofMsg.getType() == OpenFlowMessage.OF_FEATURESRESPONSE) {
					setDpIdFromMsg(ofMsg);
				}
				if(xDpId == -1) {
					// System.out.println("MSG FROM SW: " + ofMsg);
					xProxy.sendController(ofMsg);
				} else {
					xOpenFlowHandler.handleOpenFlowMessageFromSwitch(xProxy, ofMsg);
				}
			}
		}
	}
	
	class ControllerConnectionThread implements Runnable
	{
		private ProxyConnection xProxy;

		ControllerConnectionThread(ProxyConnection conn)
		{
			xProxy = conn;
		}
		
		public void run()
		{
			while(xRunning) {
				OpenFlowMessage ofMsg = xFromControllerStream.read();
				if(ofMsg == null) {
					xProxy.disconnect();
					return;
				}
				if(ofMsg.getType() == OpenFlowMessage.OF_HELLO) {
					xControllerVersion = ofMsg.getVersion();
				}
				if(xDpId == -1) {
					//System.out.println("MSG FROM CTL: " + ofMsg);
					xProxy.sendSwitch(ofMsg);
				} else {
					xOpenFlowHandler.handleOpenFlowMessageFromController(xProxy, ofMsg);
				}
			}
		}
	}
}
