/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;


/**
 * Rosco test for queuee throughput and latency
 *
 */

public class RoscoQueueTest
{
	public static final int MSG_SIZE = 300;
	public static final long WAIT_TIME = 10000;
	public static final int BATCH_TIMEOUT = 10000;

	public static void main(String[] args)
	{
		if(args.length != 3) {
			System.out.println("Usage: java RoscoQueue <id> <batch_size> <batch_count>");
			System.exit(0);
		}

		int id = Integer.parseInt(args[0]);
		int max_batch = Integer.parseInt(args[1]);
		int batch_count = Integer.parseInt(args[2]);

		try {
			RoscoConfigProperties roscoConfig = new RoscoConfigProperties(id);
			RoscoEventQueue queue = new RoscoEventQueue(id, roscoConfig, null);
			queue.start();

			if(id == 0) {
				Thread.sleep(WAIT_TIME);

				long[] lat_readings = new long[batch_count];
				long[] total_readings = new long[batch_count];

				OpenFlowMessage[] msgs = new OpenFlowMessage[max_batch];
				RandomString rs = new RandomString(MSG_SIZE);
				for(int i = 0; i < msgs.length; i++) {
					msgs[i] = new RandomOpenFlowMessage(rs.nextString());
				}
				
				int mid = 0;
				for(int j = 0; j < batch_count; j++) {
					long start = System.currentTimeMillis();
					for(int i = 0; i < msgs.length; i++) {
						queue.enqueueEvent(new RoscoEvent(mid, 0, msgs[i]));
						mid++;
					}
					int count = 0;
					while(count < max_batch) {
						queue.dequeueEvent(0);
						if(count == 0) {
							lat_readings[j] = System.currentTimeMillis() - start;
						}
						count++;
					}
					total_readings[j] = System.currentTimeMillis() - start;
				}
				System.out.print("LAT: " + max_batch);
				for(int i = 0; i < lat_readings.length; i++) {
					System.out.print("," + lat_readings[i]);
				}
				System.out.println();
				System.out.print("TOT: " + max_batch);
				for(int i = 0; i < total_readings.length; i++) {
					System.out.print("," + total_readings[i]);
				}
				System.out.println();
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
