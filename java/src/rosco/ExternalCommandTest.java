/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;

import java.util.concurrent.TimeUnit;

/**
 * Rosco test for external command execution
 *
 */

public class ExternalCommandTest
{
	public static void main(String[] args)
	{
		/*
		String cmdString = "ls";
		String[] cmdArgs = {"-a", "-l"};
		ExternalCommand cmd = new ExternalCommand(cmdString, cmdArgs);
		cmd.runCommandOrExit();
		String cmdOut = cmd.readLineFromStdOutOrExit();
		while(cmdOut != null) {
			System.out.println(cmdOut);
			cmdOut = cmd.readLineFromStdOutOrExit();
		}
		cmd.terminateCommand(10, TimeUnit.MILLISECONDS, false);
		*/
		
		String cmdString = "cat";
		ExternalCommand cmd = new ExternalCommand(cmdString);
		cmd.runCommandOrExit();
		for(int i = 0; i < 100; i++) {
			cmd.writeToStdInOrExit(i + " TEST\n");
		}
		for(int i = 0; i < 100; i++) {
			System.out.println(cmd.readLineFromStdOutOrExit());
		}
		System.out.println(cmd.terminateCommand(10, TimeUnit.MILLISECONDS, false));
	}
}
