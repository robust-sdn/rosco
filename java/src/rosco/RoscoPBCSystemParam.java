/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
W
limitations under the License.
*/
package rosco;

import java.io.BufferedReader;
import java.io.FileReader;

import it.unisa.dia.gas.jpbc.*;
import it.unisa.dia.gas.plaf.jpbc.pairing.PairingFactory;

/**
 * System parameter handler for PBC and DKG operations
 *
 */

public class RoscoPBCSystemParam
{
	private Pairing xPairing;
	private Element xU;
	private int xN;
	private int xT;
	private int xF;
	
	// Ensure the libjpbc-pbc is loaded
	static {
		System.load(System.getProperty("libjpbc.path"));
	}

	public RoscoPBCSystemParam(String sys_param, String pairing_param) throws Exception
	{
		PairingFactory.getInstance().setUsePBCWhenPossible(true);
		xPairing = PairingFactory.getPairing(pairing_param);
		BufferedReader sysParamReader = new BufferedReader(new FileReader(sys_param));

		String inputLine = sysParamReader.readLine();
		while(inputLine != null) {
			String[] contents = inputLine.split(" ");
			if(contents.length > 1) {
				switch(contents[0]) {
					case "n":
						xN = Integer.parseInt(contents[1]);
						break;
					case "t":
						xT = Integer.parseInt(contents[1]);
						break;
					case "f":
						xF = Integer.parseInt(contents[1]);
						break;
					case "U":
						xU = xPairing.getG1().newElement();
						xU.setFromString(contents[1], 10);
						break;
				}
			}
			inputLine = sysParamReader.readLine();
		}
	}

	public Pairing getPairing() { return xPairing; }
	public Element getU() { return xU; }
	public int getN() { return xN; }
	public int getT() { return xT; }
	public int getF() { return xF; }
}
