/**
  Copyright (c) 2018 James Lembke and the authors indicated in the @author tags

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package rosco;


/**
 * Rosco message queue replica that implements a malicious controller.
 *
 */

public class RoscoMalicious
{
	public static final int MSG_SIZE = 300;
	public static final long WAIT_TIME = 10000;

	public static void main(String[] args)
	{
		if(args.length != 1) {
			System.out.println("Usage: java RoscoMalicious <id>");
			System.exit(0);
		}

		int id = Integer.parseInt(args[0]);

		try {
			RoscoConfigProperties roscoConfig = new RoscoConfigProperties(id);
			RoscoEventQueue queue = new RoscoEventQueue(id, roscoConfig, null);
			queue.start();

			Thread.sleep(WAIT_TIME);

			RandomString rs = new RandomString(MSG_SIZE);
			OpenFlowMessage msg = new RandomOpenFlowMessage(rs.nextString());
		
			while(true) {
				queue.enqueueEvent(new RoscoEvent(0, id, msg));
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
